FROM --platform=linux/amd64 node:16.13-alpine3.13 as stageBuild

ADD package*.json /tmp/

RUN cd /tmp && npm install

RUN mkdir /app && cp -a /tmp/node_modules /app

WORKDIR /app

ADD . .

RUN npm run build

FROM --platform=linux/amd64 node:16.13-alpine3.13

ARG ENV_URL

WORKDIR /app

COPY --from=stageBuild /app/node_modules ./node_modules

COPY --from=stageBuild /app/dist ./dist

COPY package.json .

ADD $ENV_URL .

CMD ["npm", "run", "serve"]