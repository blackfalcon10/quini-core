import { Application } from 'express'
import yenv from 'yenv'
import IBootstrap from './interfaces/bootstrap.interface'
import http from 'http'
import Logger from '../helpers/logger.helper'

const env = yenv()
export default class ServerBootstrap implements IBootstrap {
  constructor(private app: Application) {}

  async initialize(): Promise<any> {
    return new Promise((resolve, reject) => {
      const port = env.PORT || 3000

      const server: http.Server = http.createServer(this.app)

      server
        .listen(port)
        .on('listening', () => {
          resolve(true)
          Logger.info(`Server is listening on port ${port}`)
        })
        .on('error', (error: any) => {
          reject(error)
          Logger.error(error)
        })
    })
  }
}
