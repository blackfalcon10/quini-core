import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm'
import Match from './match.entity'
import Matchday from './matchday.entity'
import Team from './team.entity'
import User from './user.entity'

export enum StatusForecast {
  SUCCESSFUL = 'successful',
  SUCCESS = 'success',
  FAILURE = 'failure',
  PENDING = 'pending',
}

@Entity({ name: 'forecast' })
@Index(['match', 'matchday'])
export default class Forecast {
  @PrimaryGeneratedColumn()
  id: number

  @Column('simple-json')
  local: number

  @Column('simple-json')
  visitor: number

  @Column({
    type: 'enum',
    enum: StatusForecast,
    default: StatusForecast.PENDING,
  })
  status: StatusForecast

  @Index()
  @ManyToOne(() => Match, (match) => match.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  match: Match

  @Index()
  @ManyToOne(() => Matchday, (matchday) => matchday.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  matchday: Matchday

  @Index()
  @ManyToOne(() => User, (user) => user.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: User

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn()
  updateAt: Date
}
