import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import League from './league.entity'
import User from './user.entity'

@Entity({ name: 'payment' })
export default class Payment {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  amount: number

  @Index()
  @ManyToOne(() => League, (league) => league.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  league: League

  @Index()
  @ManyToOne(() => User, (user) => user.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: User

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn()
  updateAt: Date
}
