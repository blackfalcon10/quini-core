import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm'
import Matchday from './matchday.entity'
import Team from './team.entity'
@Entity({ name: 'match' })
export default class Match {
  @PrimaryGeneratedColumn()
  id: number

  @Column('simple-json')
  local: { teamId: Team; result: number }

  @Column('simple-json')
  visitor: { teamId: Team; result: number }

  @Column({ type: 'timestamp' })
  endDate: Date

  @Column({ type: 'boolean', default: false, nullable: false })
  status: boolean

  @Index()
  @ManyToOne(() => Matchday, (matchday) => matchday.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  matchday: Matchday
}
