import {
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  Index,
} from 'typeorm'
import League from './league.entity'
import Match from './match.entity'

@Entity({ name: 'matchday' })
export default class Matchday {
  @PrimaryGeneratedColumn()
  id: number

  @Column()
  num: number

  @Column({ nullable: true })
  totalGoals: number

  @Column({ type: 'boolean', default: false, nullable: false })
  status: boolean

  @Index()
  @ManyToOne(() => League, (league) => league.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  league: League

  @OneToMany(() => Match, (match) => match.matchday, {
    onDelete: 'CASCADE',
  })
  match: Match
}
