import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm'
import Matchday from './matchday.entity'

@Entity({ name: 'league' })
export default class League {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'varchar', length: 50, unique: true })
  name: string

  @Column()
  price: number

  @Column({ type: 'varchar', length: 199 })
  image: string

  @Column({ type: 'boolean', default: false, nullable: false })
  status: boolean

  @Column({ type: 'boolean', default: true, nullable: false })
  isFree: boolean

  @Column({ type: 'json', nullable: true })
  rules: {
    totalMatchday: number
    totalMatch: number
    priceOfMatchday: number
    inscription: number
    endDate: Date
  }

  @OneToMany(() => Matchday, (matchday) => matchday.league, {
    onDelete: 'CASCADE',
  })
  matchday: Matchday
}
