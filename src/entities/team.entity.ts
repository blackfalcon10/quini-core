import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'team' })
export default class Team {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'varchar', length: 50, unique: true })
  name: string

  @Column({ type: 'varchar', length: 50 })
  shield: string
}
