import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  Index,
} from 'typeorm'
import Matchday from './matchday.entity'
import User from './user.entity'

@Entity({ name: 'score' })
@Index(['user', 'matchday'])
export default class Score {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ nullable: false })
  position: number

  @Column()
  win: number

  @Column()
  lost: number

  @Column()
  score: number

  @Index()
  @ManyToOne(() => User, (user) => user.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  user: User

  @Index()
  @ManyToOne(() => Matchday, (matchday) => matchday.id, {
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  matchday: Matchday
}
