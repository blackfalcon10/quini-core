import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

@Entity({ name: 'user' })
export default class User {
  @PrimaryGeneratedColumn()
  id: number

  @Column({ type: 'varchar', length: 50 })
  name: string

  @Column({ type: 'varchar', length: 50, unique: true })
  email: string

  @Column({ type: 'varchar', length: 200, nullable: true })
  password?: string

  @Column({ type: 'varchar', length: 100, nullable: true })
  refreshToken: string

  @Column({ type: 'varchar', length: 250, nullable: true })
  avatar: string

  @Column({ type: 'varchar', length: 100, nullable: true })
  providerId: string

  @Column({ type: 'varchar', length: 50, nullable: true })
  provider: string

  @Column({ type: 'boolean', default: false })
  status: boolean

  @CreateDateColumn()
  createAt: Date

  @UpdateDateColumn()
  updateAt: Date
}
