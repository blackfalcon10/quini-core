export default class ResponseDto<T> {
  static format(data: any, total: number = null) {
    if (total && total !== 0) {
      return {
        payload: { data, total },
      }
    }
    return { payload: { data } }
  }
}
