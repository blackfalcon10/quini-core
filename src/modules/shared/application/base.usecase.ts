import RepositoryBase from './base.repository'
import Result from './result.interface'

export default class UseCaseBase<T, U extends RepositoryBase<T>> {
  constructor(public operation: U) {
    this.operation = operation
  }

  async list(
    query: object = {},
    relations: string[] = [],
    order: object = {},
    fieldsToDelete: string[] = []
  ): Promise<Result<T>> {
    const where = { ...query }
    return this.operation.list(where, relations, order, fieldsToDelete)
  }

  async listOne(
    query: object = {},
    relations: string[] = []
  ): Promise<Result<T>> {
    const where = { ...query }
    return this.operation.listOne(where, relations)
  }

  async insert(entity: T): Promise<Result<T>> {
    return this.operation.insert(entity)
  }

  async update(
    entity: T,
    query: object = {},
    relations: string[] = []
  ): Promise<Result<T>> {
    const where = { ...query }
    return this.operation.update(entity, where, relations)
  }

  async remove(where: object): Promise<Result<T>> {
    return this.operation.remove(where)
  }
}
