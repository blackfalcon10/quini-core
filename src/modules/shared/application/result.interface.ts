export default interface Result<T> {
  payload: {
    data: T | T[]
    total?: number
  }
}
