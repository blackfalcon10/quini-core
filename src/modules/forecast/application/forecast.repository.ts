import MatchModel from '../../match/domain/match.model'
import MatchdayModel from '../../matchday/domain/matchday.model'
import ScoreModel from '../../score/domain/score.model'
import RepositoryBase from '../../shared/application/base.repository'
import UserModel from '../../user/domain/user.model'
import ForecastModel from '../domain/forecast.model'
import Participant from './participant.interface'

export default interface ForecastRepository
  extends RepositoryBase<ForecastModel> {
  findForecastByKeys(forecast: Partial<ForecastModel>): Promise<ForecastModel>
  findUserById(id: number): Promise<UserModel>
  findMatchdayById(id: number): Promise<MatchdayModel>
  findMatchByIds(ids: number[]): Promise<MatchModel[]>
  insertMany(forecasts: Partial<ForecastModel>[]): Promise<ForecastModel[]>
  updateMany(forecasts: Partial<ForecastModel>[]): Promise<ForecastModel[]>
  findParticipantsByMatchdayId(
    matchday: MatchdayModel
  ): Promise<ForecastModel[]>
  findUsersByIds(ids: number[]): Promise<UserModel[]>
  findScoresByMatchday(matchday: MatchdayModel): Promise<ScoreModel[]>
  saveOrUpdateMatches(matches: MatchModel[]): Promise<MatchModel[]>
  findForecastByMatchday(
    matchday: MatchdayModel,
    matches: MatchModel[]
  ): Promise<ForecastModel[]>
  findScoreByKey(score: Partial<ScoreModel>): Promise<ScoreModel>
  insertManyScore(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]>
  updateManyScore(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]>
  findMatchesByMatchdayId(matchday: MatchdayModel): Promise<MatchModel[]>
}
