import ForecastModel from '../domain/forecast.model'

export default interface Participant {
  avatar: string
  forecasts: ForecastModel[]
  name: string
  position?: number
  score?: number
  win?: number
  lost?: number
}
