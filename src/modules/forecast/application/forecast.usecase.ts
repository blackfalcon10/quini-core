import _ from 'lodash'
import moment from 'moment'
import MatchModel from '../../match/domain/match.model'
import MatchdayModel from '../../matchday/domain/matchday.model'
import ScoreModel from '../../score/domain/score.model'
import UseCaseBase from '../../shared/application/base.usecase'
import UserModel from '../../user/domain/user.model'
import ForecastModel from '../domain/forecast.model'
import ForecastRepository from './forecast.repository'
import Participant from './participant.interface'

export default class ForecastUseCase extends UseCaseBase<
  ForecastModel,
  ForecastRepository
> {
  constructor(operation: ForecastRepository) {
    super(operation)
  }

  async findUserById(id: number): Promise<UserModel> {
    return this.operation.findUserById(id)
  }
  async findMatchdayById(id: number): Promise<MatchdayModel> {
    return this.operation.findMatchdayById(id)
  }
  async findMatchByIds(ids: number[]): Promise<MatchModel[]> {
    return this.operation.findMatchByIds(ids)
  }
  async findForecastByKeys(
    forecast: Partial<ForecastModel>
  ): Promise<ForecastModel> {
    return this.operation.findForecastByKeys(forecast)
  }
  async insertMany(
    forecasts: Partial<ForecastModel>[]
  ): Promise<ForecastModel[]> {
    return this.operation.insertMany(forecasts)
  }
  async updateMany(
    forecasts: Partial<ForecastModel>[]
  ): Promise<ForecastModel[]> {
    return this.operation.updateMany(forecasts)
  }

  async updateOrCreateForecasts({
    user,
    matchday,
    matches,
    forecasts,
  }: {
    user: UserModel
    matchday: MatchdayModel
    matches: MatchModel[]
    forecasts: Partial<ForecastModel>[]
  }): Promise<ForecastModel[]> {
    const today = moment().format()
    const dataSave: Partial<ForecastModel>[] = []
    const dataUpdate: Partial<ForecastModel>[] = []
    await Promise.all(
      forecasts.map(
        async ({
          local,
          visitor,
          matchId,
        }: {
          local: number
          visitor: number
          matchId: number
        }) => {
          const match = matches.find(({ id }) => id === matchId)
          if (match) {
            const endDate = moment(match.endDate).format()
            if (today < endDate) {
              const existForecast = await this.findForecastByKeys({
                user,
                match,
                matchday,
              })
              if (existForecast) {
                dataUpdate.push({
                  id: existForecast.id,
                  local,
                  visitor,
                  match,
                  matchday,
                  user,
                })
              } else {
                dataSave.push({ local, visitor, match, matchday, user })
              }
            }
          }
        }
      )
    )
    const [save, update] = await Promise.all([
      this.insertMany(dataSave),
      this.updateMany(dataUpdate),
    ])
    return _.merge(save, update)
  }

  async listParticipantsByMatchdayId(
    matchday: MatchdayModel
  ): Promise<Participant[]> {
    const forecasts = await this.operation.findParticipantsByMatchdayId(
      matchday
    )
    if (!forecasts) {
      return []
    }
    const usersIds: number[] = forecasts.map(({ user }) => user.id)
    const users = await this.operation.findUsersByIds(usersIds)
    const scores = await this.operation.findScoresByMatchday(matchday)
    const response = users.map(({ id, name, avatar }) => {
      const userScore = scores.find(({ user }) => user.id === id)
      return {
        avatar,
        forecasts: this.getForecastsByUserId(forecasts, id),
        name,
        position: _.get(userScore, 'position', null),
        score: _.get(userScore, 'score', null),
        win: _.get(userScore, 'win', null),
        lost: _.get(userScore, 'lost', null),
      }
    })
    return _.orderBy(response, ['position'])
  }

  getForecastsByUserId(
    forecasts: ForecastModel[],
    id: number
  ): ForecastModel[] {
    return forecasts
      .filter(({ user: userForecast, match }) => {
        if (
          userForecast &&
          userForecast.id === id &&
          moment().isAfter(match.endDate)
        ) {
          return userForecast
        }
      })
      .map((forecast) => {
        const matchId = forecast.match.id
        delete forecast.user
        delete forecast.match
        return {
          ...forecast,
          matchId,
        }
      })
  }

  async finalizeProcess({
    matchday,
    matches,
  }: {
    matchday: MatchdayModel
    matches: MatchModel[]
  }) {
    let totalGoals = 0
    const dataMatches = matches.map((match) => {
      totalGoals += match.local.result
      totalGoals += match.visitor.result
      return {
        ...match,
        status: true,
      }
    })
    const saveMatches: MatchModel[] = await this.operation.saveOrUpdateMatches(
      dataMatches
    )
    const forecasts = await this.operation.findForecastByMatchday(
      matchday,
      saveMatches
    )
    const users: UserModel[] = _.uniqBy(
      forecasts.map(({ user }) => user),
      'id'
    )
    const { updateForecast, scores, isComplete } = await this.abilityCheckOver({
      forecasts,
      matchday,
      totalGoals,
      saveMatches,
      users,
    })

    const [setForecasts, setScores] = await Promise.all([
      this.updateMany(updateForecast),
      this.updateOrCreateScores({
        scores,
        users,
        matchday,
        isComplete,
      }),
    ])

    return { setForecasts, setScores }
  }

  async abilityCheckOver({
    forecasts,
    matchday,
    totalGoals,
    saveMatches,
    users,
  }: {
    forecasts: ForecastModel[]
    matchday: MatchdayModel
    totalGoals: number
    saveMatches: MatchModel[]
    users: UserModel[]
  }) {
    const isComplete = await this.isComplete(saveMatches, matchday)
    const updateForecast: ForecastModel[] = []
    const scores: ScoreModel[] = []
    await Promise.all(
      users.map((user) => {
        const forecastsByUser = this.getForecastsByUser(forecasts, user)
        let score = 0
        let win = 0
        let lost = 0
        let goal = 0
        forecastsByUser.forEach((forecast) => {
          const match = saveMatches.find(({ id }) => id === forecast.match.id)
          const finalLocal = _.get(match, 'local.result')
          const finalVisitor = _.get(match, 'visitor.result')
          const resultLocal = _.get(forecast, 'local')
          const resultVisitor = _.get(forecast, 'visitor')
          if (
            this.winWithResult({
              finalLocal,
              resultLocal,
              finalVisitor,
              resultVisitor,
            })
          ) {
            score += 25
            win += 1
            updateForecast.push({ ...forecast, status: 'successful' })
          } else if (
            this.winWithoutResult({
              finalLocal,
              resultLocal,
              finalVisitor,
              resultVisitor,
            })
          ) {
            score += 10
            win += 1
            updateForecast.push({ ...forecast, status: 'success' })
          } else {
            lost += 1
            updateForecast.push({ ...forecast, status: 'failure' })
          }

          goal += resultLocal + resultVisitor
        })

        if (this.isCompletedAndTotalGoals({ isComplete, totalGoals, goal })) {
          score += 25
        }
        scores.push({
          score,
          win,
          lost,
          user,
          matchday,
          position: null,
        })
      })
    )
    return { updateForecast, scores, isComplete }
  }

  getForecastsByUser(forecasts: ForecastModel[], user: UserModel) {
    return forecasts.filter(
      ({ user: userForecast }) => userForecast.id === user.id
    )
  }

  winWithResult({
    finalLocal,
    resultLocal,
    finalVisitor,
    resultVisitor,
  }: {
    finalLocal: number
    resultLocal: number
    finalVisitor: number
    resultVisitor: number
  }) {
    return finalLocal === resultLocal && finalVisitor === resultVisitor
  }

  winWithoutResult({
    finalLocal,
    resultLocal,
    finalVisitor,
    resultVisitor,
  }: {
    finalLocal: number
    resultLocal: number
    finalVisitor: number
    resultVisitor: number
  }) {
    return (
      (finalLocal > finalVisitor && resultLocal > resultVisitor) ||
      (finalLocal < finalVisitor && resultLocal < resultVisitor) ||
      (finalLocal === finalVisitor && resultLocal === resultVisitor)
    )
  }

  async isComplete(saveMatches: MatchModel[], matchday: MatchdayModel) {
    const matches = await this.operation.findMatchesByMatchdayId(matchday)
    return matches.length === saveMatches.length
  }

  isCompletedAndTotalGoals({
    isComplete,
    totalGoals,
    goal,
  }: {
    isComplete: boolean
    totalGoals: number
    goal: number
  }) {
    return isComplete && totalGoals === goal
  }

  isCompleteAndFirstPlace(isComplete: boolean, position: number) {
    return isComplete && position === 1
  }

  async updateOrCreateScores({
    scores,
    users,
    matchday,
    isComplete,
  }: {
    scores: ScoreModel[]
    users: UserModel[]
    matchday: MatchdayModel
    isComplete: boolean
  }): Promise<ScoreModel[]> {
    const dataSave: Partial<ScoreModel>[] = []
    const dataUpdate: Partial<ScoreModel>[] = []

    const orderScores = _.orderBy(scores, ['win', 'score'], ['desc', 'desc'])
    let position = 1
    for (const score of orderScores) {
      const user = users.find(({ id }) => id === score.user.id)
      if (user) {
        const existScore = await this.operation.findScoreByKey({
          user: user,
          matchday: matchday,
        })
        if (this.isCompleteAndFirstPlace(isComplete, position)) {
          score.score += 50
        }
        if (existScore) {
          dataUpdate.push({
            id: existScore.id,
            ...score,
            user,
            matchday,
            position,
          })
        } else {
          dataSave.push({
            ...score,
            user,
            matchday,
            position,
          })
        }
        position++
      }
    }
    const [save, update] = await Promise.all([
      this.operation.insertManyScore(dataSave),
      this.operation.updateManyScore(dataUpdate),
    ])
    return _.merge(save, update)
  }
}
