import _ from 'lodash'
import { getRepository, Repository, In, LessThan } from 'typeorm'
import Forecast from '../../../entities/forecast.entity'
import Match from '../../../entities/match.entity'
import Matchday from '../../../entities/matchday.entity'
import Score from '../../../entities/score.entity'
import User from '../../../entities/user.entity'
import MatchModel from '../../match/domain/match.model'
import MatchdayModel from '../../matchday/domain/matchday.model'
import ScoreModel from '../../score/domain/score.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import UserModel from '../../user/domain/user.model'
import ForecastModel from '../domain/forecast.model'

export default class ForecastOperation extends OperationBase<ForecastModel> {
  constructor() {
    super(Forecast)
  }

  async findForecastByKeys(
    forecast: Partial<ForecastModel>
  ): Promise<ForecastModel> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    const where = forecast
    return repository.findOne(where)
  }

  async findUserById(id: number): Promise<UserModel> {
    const repository: Repository<UserModel> = getRepository(User)
    return repository.findOne(id)
  }
  async findMatchdayById(id: number): Promise<MatchdayModel> {
    const repository: Repository<MatchdayModel> = getRepository(Matchday)
    return repository.findOne(id)
  }
  async findMatchByIds(ids: number[]): Promise<MatchModel[]> {
    const repository: Repository<MatchModel> = getRepository(Match)
    return repository.find({
      id: In(ids),
    })
  }

  async insertMany(
    forecasts: Partial<ForecastModel>[]
  ): Promise<ForecastModel[]> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    return repository.save(forecasts)
  }

  async updateMany(
    forecasts: Partial<ForecastModel>[]
  ): Promise<ForecastModel[]> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    let recordToUpdate = await repository.find({
      id: In(forecasts.map(({ id }) => id)),
    })
    recordToUpdate = _.merge(recordToUpdate, forecasts)

    return repository.save(recordToUpdate)
  }

  async findParticipantsByMatchdayId(
    matchday: MatchdayModel
  ): Promise<ForecastModel[]> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    const where = {
      matchday,
    }
    return repository.find({
      where,
      select: ['id', 'local', 'visitor', 'status'],
      relations: ['user', 'match'],
      order: {
        match: 'ASC',
      },
    })
  }

  async findUsersByIds(ids: number[]): Promise<UserModel[]> {
    const repository: Repository<UserModel> = getRepository(User)
    return repository.find({
      id: In(ids),
    })
  }
  async findScoresByMatchday(matchday: MatchdayModel): Promise<ScoreModel[]> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    return repository.find({
      where: {
        matchday,
      },
      relations: ['user'],
      order: {
        win: 'DESC',
        score: 'DESC',
      },
    })
  }

  async saveOrUpdateMatches(matches: MatchModel[]): Promise<MatchModel[]> {
    const repository: Repository<MatchModel> = getRepository(Match)
    let recordToUpdate = await repository.find({
      id: In(matches.map(({ id }) => id)),
    })
    recordToUpdate = _.merge(recordToUpdate, matches)

    return repository.save(recordToUpdate)
  }

  async findForecastByMatchday(
    matchday: MatchdayModel,
    matches: MatchModel[]
  ): Promise<ForecastModel[]> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    const where = {
      matchday,
      match: In(matches.map(({ id }) => id)),
    }
    return repository.find({
      where,
      relations: ['user', 'match'],
    })
  }

  async findScoreByKey(score: ScoreModel): Promise<ScoreModel> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    const where = score
    return repository.findOne(where)
  }

  async insertManyScore(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    return repository.save(scores)
  }

  async updateManyScore(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    let recordToUpdate = await repository.find({
      id: In(scores.map(({ id }) => id)),
    })
    recordToUpdate = _.merge(recordToUpdate, scores)

    return repository.save(recordToUpdate)
  }

  async findMatchesByMatchdayId(
    matchday: MatchdayModel
  ): Promise<MatchModel[]> {
    const repository: Repository<MatchModel> = getRepository(Match)
    const where = { matchday: matchday }
    return repository.find(where)
  }
}
