import MatchModel from '../../match/domain/match.model'
import MatchdayModel from '../../matchday/domain/matchday.model'
import UserModel from '../../user/domain/user.model'

export default interface ForecastModel {
  id?: number
  local: number
  visitor: number
  status: string
  match: MatchModel
  matchday: MatchdayModel
  user: UserModel
  createAt: Date
  updateAt: Date
}
