import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import ForecastUseCase from '../application/forecast.usecase'
import ForecastOperation from '../infraestructure/forecast.operation'
import ForecastController from './forecast.controller'
import ForecastSchema from './forecast.schema'

const operation = new ForecastOperation()
const useCase = new ForecastUseCase(operation)
const controller = new ForecastController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ForecastSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.get(
  '/:id/participants',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ForecastSchema.LIST_ONE),
  ErrorHandler.asyncError(
    controller.listParticipantsByMatchdayId.bind(controller)
  )
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ForecastSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)

route.post(
  '/end',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ForecastSchema.INSERT_END.body),
  ErrorHandler.asyncError(controller.insertEnd.bind(controller))
)

export { route }
