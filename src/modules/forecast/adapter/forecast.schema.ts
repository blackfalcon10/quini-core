import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const forecast = Joi.object().keys({
  local: Joi.number().required(),
  visitor: Joi.number().required(),
  status: Joi.boolean(),
  matchId: Joi.number().required(),
})

const matches = Joi.object().keys({
  local: Joi.object({
    teamId: Joi.number().required(),
    result: Joi.number().allow(null),
  }).required(),
  visitor: Joi.object({
    teamId: Joi.number().required(),
    result: Joi.number().allow(null),
  }).required(),
  id: Joi.number().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      matchdayId: Joi.number().required(),
      forecasts: Joi.array().items(forecast).required(),
      auth: Joi.object().required(),
    }),
  },
  INSERT_END: {
    body: Joi.object({
      matchdayId: Joi.number().required(),
      matches: Joi.array().items(matches).required(),
      auth: Joi.object().required(),
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      matchdayId: Joi.number().required(),
      forecasts: Joi.array().items(forecast).required(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
