import ForecastUseCase from '../application/forecast.usecase'
import { Request, Response } from 'express'
import ForecastModel from '../domain/forecast.model'

export default class ForecastController {
  constructor(private useCase: ForecastUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list({})
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const matchdayId = +req.params.id
    const { id: userId } = res.locals.auth
    const [user, matchday] = await Promise.all([
      this.useCase.findUserById(userId),
      this.useCase.findMatchdayById(matchdayId),
    ])
    const forecast: Partial<ForecastModel> = { user, matchday }
    const result = await this.useCase.list(forecast, ['match'])
    res.json(result)
  }

  async listParticipantsByMatchdayId(req: Request, res: Response) {
    const matchdayId = +req.params.id
    const matchday = await this.useCase.findMatchdayById(matchdayId)
    if (!matchday) {
      return res.status(400).send({ message: 'matchdayId not found' })
    }
    const result = await this.useCase.listParticipantsByMatchdayId(matchday)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const { matchdayId, forecasts } = req.body
    const { id: userId } = res.locals.auth
    if (!userId) {
      return res.status(400).send({ message: 'UserId not found' })
    }
    const matchIds = forecasts.map((forecast: any) => forecast.matchId)
    const [user, matchday, matches] = await Promise.all([
      this.useCase.findUserById(userId),
      this.useCase.findMatchdayById(matchdayId),
      this.useCase.findMatchByIds(matchIds),
    ])
    if (!user) {
      return res.status(400).send({ message: 'UserId not found' })
    }
    if (!matchday) {
      return res.status(400).send({ message: 'matchdayId not found' })
    }
    if (!matches) {
      return res.status(400).send({ message: 'matches not found' })
    }

    res.json({
      status: true,
      msg: 'Tu quiniela se ha guardado correctamente.',
      forecasts: await this.useCase.updateOrCreateForecasts({
        user,
        matchday,
        matches,
        forecasts,
      }),
    })
  }

  async insertEnd(req: Request, res: Response) {
    const { matchdayId, matches } = req.body
    const matchday = await this.useCase.findMatchdayById(matchdayId)

    if (!matchday) {
      return res.status(400).send({ message: 'matchdayId not found' })
    }

    const response = await this.useCase.finalizeProcess({
      matches,
      matchday,
    })

    res.json({
      status: true,
      msg: 'Los resultados de la quiniela se ha guardado correctamente.',
      response,
    })
  }
}
