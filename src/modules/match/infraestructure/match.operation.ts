import { getRepository, Repository } from 'typeorm'
import Match from '../../../entities/match.entity'
import Matchday from '../../../entities/matchday.entity'
import MatchdayModel from '../../matchday/domain/matchday.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import MatchModel from '../domain/match.model'

export default class MatchOperation extends OperationBase<MatchModel> {
  constructor() {
    super(Match)
  }

  async findMatchByKey(key: string, value: string): Promise<MatchModel> {
    const repository: Repository<MatchModel> = getRepository(Match)
    const where = { [key]: value }
    return repository.findOne(where)
  }

  async findMatchdayById(id: number): Promise<MatchdayModel> {
    const repository: Repository<MatchdayModel> = getRepository(Matchday)
    return repository.findOne(id)
  }

  async insertMany(matches: Partial<MatchModel>[]): Promise<MatchModel[]> {
    const repository: Repository<MatchModel> = getRepository(Match)
    return repository.save(matches)
  }
}
