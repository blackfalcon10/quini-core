import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import MatchUseCase from '../application/match.usecase'
import MatchOperation from '../infraestructure/match.operation'
import MatchController from './match.controller'
import MatchSchema from './match.schema'

const operation = new MatchOperation()
const useCase = new MatchUseCase(operation)
const controller = new MatchController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
route.put(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchSchema.UPDATE.body),
  controller.update.bind(controller)
)
route.delete(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchSchema.REMOVE.params),
  ErrorHandler.asyncError(controller.remove.bind(controller))
)

export { route }
