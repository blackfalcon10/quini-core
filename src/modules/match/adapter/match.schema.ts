import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const match = Joi.object().keys({
  local: Joi.object({
    teamId: Joi.number().required(),
    result: Joi.number().allow(null),
  }).required(),
  visitor: Joi.object({
    teamId: Joi.number().required(),
    result: Joi.number().allow(null),
  }).required(),
  endDate: Joi.date().required(),
  status: Joi.boolean(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      matches: Joi.array().items(match).required(),
      matchdayId: Joi.number().required(),
      auth: Joi.object().required(),
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      matches: Joi.array().items(match).required(),
      matchdayId: Joi.number().required(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
