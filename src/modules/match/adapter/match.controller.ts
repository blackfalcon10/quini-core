import MatchUseCase from '../application/match.usecase'
import { Request, Response } from 'express'
import MatchModel from '../domain/match.model'

export default class MatchController {
  constructor(private useCase: MatchUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const match: Partial<MatchModel> = { id }
    const result = await this.useCase.listOne(match)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const { matchdayId, matches } = req.body
    const matchday = await this.useCase.findMatchdayById(matchdayId)
    if (!matchday) {
      return res.status(400).send({ message: 'matchdayId not found' })
    }
    const result = await this.useCase.insertMany(matches, matchday)
    res.json(result)
  }

  async update(req: Request, res: Response) {
    const id = req.params.id
    const validateId = await this.useCase.findMatchByKey('id', id)
    if (!validateId) {
      return res.status(400).send({ message: 'Id not found' })
    }
    const match: MatchModel = req.body
    const result = await this.useCase.update(match, { id })
    res.json(result)
  }

  async remove(req: Request, res: Response) {
    const params = req.params
    const id = +params.id
    const result = await this.useCase.remove({ id })
    res.json(result)
  }
}
