import TeamModel from '../../team/domain/team.model'

export default interface MatchResultModel {
  teamId: TeamModel
  result?: number
}
