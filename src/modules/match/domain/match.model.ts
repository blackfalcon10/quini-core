import MatchdayModel from '../../matchday/domain/matchday.model'
import MatchResultModel from './match.result.model'

export default interface MatchModel {
  id?: number
  local: MatchResultModel
  visitor: MatchResultModel
  endDate: Date
  status?: boolean
  matchday: MatchdayModel
}
