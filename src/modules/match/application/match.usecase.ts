import moment from 'moment-timezone'
import MatchdayModel from '../../matchday/domain/matchday.model'
import UseCaseBase from '../../shared/application/base.usecase'
import MatchModel from '../domain/match.model'
import MatchRepository from './match.repository'

export default class MatchUseCase extends UseCaseBase<
  MatchModel,
  MatchRepository
> {
  constructor(operation: MatchRepository) {
    super(operation)
  }

  async findMatchdayById(id: number): Promise<MatchdayModel> {
    return this.operation.findMatchdayById(id)
  }

  async findMatchByKey(key: string, value: string): Promise<MatchModel> {
    return this.operation.findMatchByKey(key, value)
  }

  async insertMany(
    matches: Partial<MatchModel>[],
    matchday: MatchdayModel
  ): Promise<MatchModel[]> {
    const saveMatches: Partial<MatchModel>[] = matches.map((match) => {
      const endDateMx = moment.tz(match.endDate, 'America/Mexico_City')
      return {
        ...match,
        endDate: endDateMx.utc().toDate(),
        matchday,
      }
    })
    return this.operation.insertMany(saveMatches)
  }
}
