import MatchdayModel from '../../matchday/domain/matchday.model'
import RepositoryBase from '../../shared/application/base.repository'
import MatchModel from '../domain/match.model'

export default interface MatchRepository extends RepositoryBase<MatchModel> {
  findMatchByKey(key: string, value: string): Promise<MatchModel>
  findMatchdayById(id: number): Promise<MatchdayModel>
  insertMany(matches: Partial<MatchModel>[]): Promise<MatchModel[]>
}
