import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const rules = Joi.object({
  totalMatchday: Joi.number().required(),
  totalMatch: Joi.number().required(),
  priceOfMatchday: Joi.number().required(),
  inscription: Joi.number().required(),
  endDate: Joi.string().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      name: Joi.string().required(),
      price: Joi.number().required(),
      image: Joi.string().required(),
      status: Joi.boolean(),
      isFree: Joi.boolean(),
      auth: Joi.object().required(),
      rules,
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      name: Joi.string(),
      price: Joi.number().required(),
      image: Joi.string().required(),
      status: Joi.boolean(),
      isFree: Joi.boolean(),
      auth: Joi.object().required(),
      rules,
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
