import LeagueUseCase from '../application/league.usecase'
import { Request, Response } from 'express'
import LeagueModel from '../domain/league.model'

export default class LeagueController {
  constructor(private useCase: LeagueUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listLast(req: Request, res: Response) {
    const result = await this.useCase.findLast()
    res.json(result)
  }

  async listTables(req: Request, res: Response) {
    const result = await this.useCase.findTables()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const league: Partial<LeagueModel> = { id }
    const result = await this.useCase.listOne(league)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const body = req.body
    const league: LeagueModel = {
      name: body.name,
      price: body.price,
      image: body.image,
      status: body.status,
      isFree: body.isFree,
      rules: body.rules,
    }
    const leagueAlreadyExist = await this.useCase.findLeagueByKey(
      'name',
      league.name
    )
    if (leagueAlreadyExist) {
      return res.status(400).send({ message: 'League already exists' })
    }
    const result = await this.useCase.insert(league)
    res.json(result)
  }

  async update(req: Request, res: Response) {
    const id = req.params.id

    const body = req.body
    const league: LeagueModel = {
      name: body.name,
      price: body.price,
      image: body.image,
      status: body.status,
      isFree: body.isFree,
      rules: body.rules,
    }

    const validateId = await this.useCase.findLeagueByKey('id', id)
    if (!validateId) {
      return res.status(400).send({ message: 'Id not found' })
    }
    const leagueAlreadyExistByName = await this.useCase.findLeagueByKey(
      'name',
      league.name
    )
    if (leagueAlreadyExistByName) {
      return res.status(400).send({ message: 'League already exists' })
    }
    const result = await this.useCase.update(league, { id })
    res.json(result)
  }

  async remove(req: Request, res: Response) {
    const params = req.params
    const id = +params.id
    const result = await this.useCase.remove({ id })
    res.json(result)
  }
}
