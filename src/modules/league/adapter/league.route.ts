import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import LeagueUseCase from '../application/league.usecase'
import LeagueOperation from '../infraestructure/league.operation'
import LeagueController from './league.controller'
import LeagueSchema from './league.schema'

const operation = new LeagueOperation()
const useCase = new LeagueUseCase(operation)
const controller = new LeagueController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/last',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.listLast.bind(controller))
)
route.get(
  '/tables',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.listTables.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(LeagueSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(LeagueSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
route.put(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(LeagueSchema.UPDATE.body),
  controller.update.bind(controller)
)
route.delete(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(LeagueSchema.REMOVE.params),
  ErrorHandler.asyncError(controller.remove.bind(controller))
)

export { route }
