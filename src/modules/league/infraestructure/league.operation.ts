import { getRepository, Repository } from 'typeorm'
import League from '../../../entities/league.entity'
import Payment from '../../../entities/payment.entity'
import PaymentModel from '../../payment/domain/payment.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import { ScoresTables } from '../application/table.interface'
import LeagueModel from '../domain/league.model'

export default class LeagueOperation extends OperationBase<LeagueModel> {
  constructor() {
    super(League)
  }

  async findLeagueByKey(key: string, value: string): Promise<LeagueModel> {
    const repository: Repository<LeagueModel> = getRepository(League)
    const where = { [key]: value }
    return repository.findOne(where)
  }

  async findLast(): Promise<LeagueModel> {
    const repository: Repository<LeagueModel> = getRepository(League)
    return repository.findOne({
      order: {
        id: -1,
      },
      relations: ['matchday'],
    })
  }

  async findTables(): Promise<ScoresTables[]> {
    const repository: Repository<LeagueModel> = getRepository(League)
    const { id } = await repository.findOne({
      order: {
        id: -1,
      },
    })

    const repositoryPayment: Repository<PaymentModel> = getRepository(Payment)
    const payments = await repositoryPayment.find({
      where: {
        league: <Partial<LeagueModel>>id,
      },
      relations: ['user'],
    })

    const tables = await repository.manager.query(`SELECT
        u.id AS id,
        u.name AS name,
        u.avatar AS avatar,
        sum( s.win ) AS wins,
        sum( s.lost ) AS lost,
        sum( s.score ) AS score,
        count( u.id ) AS jj
      FROM
        score s
        LEFT JOIN user u ON  u.id = s.userId
        LEFT JOIN matchday m ON m.id = s.matchdayId
      WHERE
        m.leagueId = ${id}
      GROUP BY
        u.id
      ORDER BY
        wins desc, score desc, name;`)

    return tables.map((table: ScoresTables) => {
      const payment = payments.filter(({ user }) => user.id === table.id)
      return {
        ...table,
        payment: payment.map(({ amount, createAt }) => {
          return {
            amount,
            createAt,
          }
        }),
      }
    })
  }
}
