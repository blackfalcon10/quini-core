export default interface LeagueModel {
  id?: number
  name: string
  price: number
  image: string
  status?: boolean
  isFree?: boolean
  rules?: {
    totalMatchday: number
    totalMatch: number
    priceOfMatchday: number
    inscription: number
    endDate: Date
  }
}
