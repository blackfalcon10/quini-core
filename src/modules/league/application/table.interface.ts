export interface ScoresTables {
  id: number
  position: number
  name: string
  avatar?: string
  wins: number
  lost: number
  score: number
  jj: number
  payment?: {
    amount: number
    createAt: Date
  }
}
