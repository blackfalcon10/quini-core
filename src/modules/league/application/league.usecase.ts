import UseCaseBase from '../../shared/application/base.usecase'
import LeagueModel from '../domain/league.model'
import LeagueRepository from './league.repository'
import { ScoresTables } from './table.interface'

export default class LeagueUseCase extends UseCaseBase<
  LeagueModel,
  LeagueRepository
> {
  constructor(operation: LeagueRepository) {
    super(operation)
  }
  async findLeagueByKey(key: string, value: string): Promise<LeagueModel> {
    return this.operation.findLeagueByKey(key, value)
  }
  async findLast(): Promise<LeagueModel> {
    return this.operation.findLast()
  }
  async findTables(): Promise<ScoresTables[]> {
    return this.operation.findTables()
  }
}
