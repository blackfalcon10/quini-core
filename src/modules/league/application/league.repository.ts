import RepositoryBase from '../../shared/application/base.repository'
import LeagueModel from '../domain/league.model'
import { ScoresTables } from './table.interface'

export default interface LeagueRepository extends RepositoryBase<LeagueModel> {
  findLeagueByKey(key: string, value: string): Promise<LeagueModel>
  findLast(): Promise<LeagueModel>
  findTables(): Promise<ScoresTables[]>
}
