export default interface UserModel {
  id?: number
  name: string
  email: string
  password?: string
  refreshToken?: string
  avatar?: string
  providerId?: string
  provider?: string
  status?: boolean
}
