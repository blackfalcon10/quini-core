import UseCaseBase from '../../shared/application/base.usecase'
import Result from '../../shared/application/result.interface'
import UserModel from '../domain/user.model'
import UserRepository from './user.repository'
import UserService from './user.service'

export default class UserUseCase extends UseCaseBase<
  UserModel,
  UserRepository
> {
  operation: UserRepository

  constructor(operation: UserRepository) {
    super(operation)
    this.operation = operation
  }

  override async insert(entity: UserModel): Promise<Result<UserModel>> {
    entity.password = await UserService.cryptPassword(entity.password)
    entity.refreshToken = UserService.generateRefreshToken()

    return this.operation.insert(entity)
  }

  async findUserByEmail(email: string): Promise<UserModel> {
    return this.operation.findUserByEmail(email)
  }
}
