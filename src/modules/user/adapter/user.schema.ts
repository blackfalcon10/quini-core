import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})
const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      name: Joi.string().required(),
      email: Joi.string().required(),
      password: Joi.string()
        .pattern(
          new RegExp(
            /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#?$%^&*])(?=.{8,})/
          )
        )
        .required(),
      avatar: Joi.string().required(),
      auth: Joi.object().required(),
    }),
  },
}

export default schemas
