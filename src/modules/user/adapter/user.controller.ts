import UserUseCase from '../application/user.usecase'
import { Request, Response } from 'express'
import UserModel from '../domain/user.model'

export default class UserController {
  constructor(private useCase: UserUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const user: Partial<UserModel> = { id }
    const result = await this.useCase.listOne(user)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const body = req.body
    const user: UserModel = {
      name: body.name,
      email: body.email.toLowerCase(),
      avatar: body.avatar,
      password: body.password,
    }
    const userAlreadyExist = await this.useCase.findUserByEmail(user.email)
    if (userAlreadyExist) {
      return res.status(400).send({ message: 'user already exists' })
    }
    const result = await this.useCase.insert(user)
    res.status(201).json(result)
  }
}
