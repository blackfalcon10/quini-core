import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import UserUseCase from '../application/user.usecase'
import UserOperation from '../infraestructure/user.operation'
import UserController from './user.controller'
import UserSchema from './user.schema'

const operation = new UserOperation()
const useCase = new UserUseCase(operation)
const controller = new UserController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)

route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(UserSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)

route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(UserSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)

export { route }
