import UserModel from '../../user/domain/user.model'

export interface AuthRepository {
  login(where: object, relations: string[]): Promise<UserModel>
  createOrUpdateUser(user: Partial<UserModel>): Promise<UserModel>
}
