export interface Tokens {
  accessToken: string
  refreshToken: string
  status: boolean
}
