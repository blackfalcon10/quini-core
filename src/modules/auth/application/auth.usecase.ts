import UserService from '../../user/application/user.service'
import UserModel from '../../user/domain/user.model'
import { AuthRepository } from './auth.repository'
import { Tokens } from './tokens.interface'

export class AuthUseCase {
  constructor(private readonly authRepository: AuthRepository) {
    this.authRepository = authRepository
  }

  async login(entity: Partial<UserModel>): Promise<Tokens> {
    const user: UserModel = await this.authRepository.login(
      {
        email: entity.email,
      },
      null
    )

    if (user) {
      const match = await UserService.decryptPassword(
        entity.password,
        user.password
      )

      if (match) {
        const accessToken = UserService.generateAccessToken(user)

        return {
          accessToken,
          refreshToken: user.refreshToken,
          status: user.status,
        }
      }

      return null
    }

    return null
  }

  async loginAuth(entity: Partial<UserModel>): Promise<Tokens> {
    entity.refreshToken = UserService.generateRefreshToken()
    const user: UserModel = await this.authRepository.createOrUpdateUser(entity)

    if (user) {
      const accessToken = UserService.generateAccessToken(user)

      return {
        accessToken,
        refreshToken: user.refreshToken,
        status: user.status,
      }
    }

    return null
  }

  async getNewAccessToken(entity: Partial<UserModel>): Promise<Tokens> {
    const user: UserModel = await this.authRepository.login(
      {
        refreshToken: entity.refreshToken,
      },
      null
    )

    if (user) {
      const accessToken = UserService.generateAccessToken(user)
      return {
        accessToken,
        refreshToken: user.refreshToken,
        status: user.status,
      }
    }

    return null
  }
}
