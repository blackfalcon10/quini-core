import _ from 'lodash'
import { Repository, getRepository } from 'typeorm'
import User from '../../../entities/user.entity'
import UserModel from '../../user/domain/user.model'
import { AuthRepository } from '../application/auth.repository'

export class AuthOperation implements AuthRepository {
  async login(where: object, relations: string[]): Promise<UserModel> {
    const repository: Repository<User> = getRepository(User)
    const data: UserModel = await repository.findOne({ where, relations })

    return data
  }

  async createOrUpdateUser(user: Partial<UserModel>): Promise<UserModel> {
    const repository: Repository<User> = getRepository(User)
    let recordToUpdate = await repository.findOne({
      where: { email: user.email },
    })
    recordToUpdate = _.merge(recordToUpdate, user)

    return repository.save(recordToUpdate)
  }
}
