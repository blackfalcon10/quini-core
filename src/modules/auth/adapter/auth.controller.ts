import { Request, Response } from 'express'
import UserModel from '../../user/domain/user.model'
import { AuthUseCase } from '../application/auth.usecase'

export class AuthController {
  unauthorized
  constructor(private useCase: AuthUseCase) {
    this.useCase = useCase
    this.unauthorized = {
      messages: 'Usuario invalido',
      status: 'Unauthorized',
    }
  }

  async login(req: Request, res: Response) {
    const { email, password } = req.body
    const user: Partial<UserModel> = {
      email,
      password,
    }

    const result = await this.useCase.login(user)
    if (result) {
      return res.json(result)
    }
    return res.status(401).json(this.unauthorized)
  }

  async loginAuth(req: Request, res: Response) {
    const { user: userBody, strategy } = req.body
    const user: Partial<UserModel> = {
      name: userBody.name,
      email: userBody.email,
      avatar: userBody.picture,
      providerId: userBody.sub,
      provider: strategy,
    }

    const result = await this.useCase.loginAuth(user)
    if (result) {
      return res.json(result)
    }
    return res.status(401).json(this.unauthorized)
  }

  async getNewAccessToken(req: Request, res: Response) {
    const { refreshToken } = req.params

    const user: Partial<UserModel> = { refreshToken }

    const result = await this.useCase.getNewAccessToken(user)
    if (result) {
      return res.json(result)
    }

    return res.status(401).json(this.unauthorized)
  }
}
