import UseCaseBase from '../../shared/application/base.usecase'
import TeamModel from '../domain/team.model'
import TeamRepository from './team.repository'

export default class TeamUseCase extends UseCaseBase<
  TeamModel,
  TeamRepository
> {
  constructor(operation: TeamRepository) {
    super(operation)
  }
  async findTeamByKey(key: string, value: string): Promise<TeamModel> {
    return this.operation.findTeamByKey(key, value)
  }
}
