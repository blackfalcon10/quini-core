import RepositoryBase from '../../shared/application/base.repository'
import TeamModel from '../domain/team.model'

export default interface TeamRepository extends RepositoryBase<TeamModel> {
  findTeamByKey(key: string, value: string): Promise<TeamModel>
}
