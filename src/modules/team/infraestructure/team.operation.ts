import { getRepository, Repository } from 'typeorm'
import Team from '../../../entities/team.entity'
import OperationBase from '../../shared/infraestructure/base.operation'
import TeamModel from '../domain/team.model'

export default class TeamOperation extends OperationBase<TeamModel> {
  constructor() {
    super(Team)
  }

  async findTeamByKey(key: string, value: string): Promise<TeamModel> {
    const repository: Repository<TeamModel> = getRepository(Team)
    const where = { [key]: value }
    return repository.findOne(where)
  }
}
