import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      name: Joi.string().required(),
      shield: Joi.string().required(),
      auth: Joi.object().required(),
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      name: Joi.string(),
      shield: Joi.string(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
