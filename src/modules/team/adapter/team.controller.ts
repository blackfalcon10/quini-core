import TeamUseCase from '../application/team.usecase'
import { Request, Response } from 'express'
import TeamModel from '../domain/team.model'

export default class TeamController {
  constructor(private useCase: TeamUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const team: Partial<TeamModel> = { id }
    const result = await this.useCase.listOne(team)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const body = req.body
    const team: TeamModel = {
      name: body.name,
      shield: body.shield,
    }
    const teamAlreadyExist = await this.useCase.findTeamByKey('name', team.name)
    if (teamAlreadyExist) {
      return res.status(400).send({ message: 'Team already exists' })
    }
    const result = await this.useCase.insert(team)
    res.json(result)
  }

  async update(req: Request, res: Response) {
    const id = req.params.id

    const body = req.body
    const team: TeamModel = {
      name: body.name,
      shield: body.shield,
    }

    const validateId = await this.useCase.findTeamByKey('id', id)
    if (!validateId) {
      return res.status(400).send({ message: 'Id not found' })
    }
    const teamAlreadyExistByName = await this.useCase.findTeamByKey(
      'name',
      team.name
    )
    if (teamAlreadyExistByName) {
      return res.status(400).send({ message: 'Team already exists' })
    }
    const result = await this.useCase.update(team, { id })
    res.json(result)
  }

  async remove(req: Request, res: Response) {
    const params = req.params
    const id = +params.id
    const result = await this.useCase.remove({ id })
    res.json(result)
  }
}
