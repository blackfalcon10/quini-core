import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import TeamUseCase from '../application/team.usecase'
import TeamOperation from '../infraestructure/team.operation'
import TeamController from './team.controller'
import TeamSchema from './team.schema'

const operation = new TeamOperation()
const useCase = new TeamUseCase(operation)
const controller = new TeamController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(TeamSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(TeamSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
route.put(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(TeamSchema.UPDATE.body),
  controller.update.bind(controller)
)
route.delete(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(TeamSchema.REMOVE.params),
  ErrorHandler.asyncError(controller.remove.bind(controller))
)

export { route }
