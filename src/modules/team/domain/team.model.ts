export default interface TeamModel {
  id?: number
  name: string
  shield: string
}
