import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      amount: Joi.number().required(),
      userId: Joi.number().required(),
      leagueId: Joi.number().required(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
