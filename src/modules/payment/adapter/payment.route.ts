import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import PaymentUseCase from '../application/payment.usecase'
import PaymentOperation from '../infraestructure/payment.operation'
import PaymentController from './payment.controller'
import PaymentSchema from './payment.schema'

const operation = new PaymentOperation()
const useCase = new PaymentUseCase(operation)
const controller = new PaymentController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(PaymentSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(PaymentSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
route.delete(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(PaymentSchema.REMOVE.params),
  ErrorHandler.asyncError(controller.remove.bind(controller))
)

export { route }
