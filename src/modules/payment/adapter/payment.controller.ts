import PaymentUseCase from '../application/payment.usecase'
import { Request, Response } from 'express'
import PaymentModel from '../domain/payment.model'

export default class PaymentController {
  constructor(private useCase: PaymentUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const payment: Partial<PaymentModel> = { id }
    const result = await this.useCase.listOne(payment)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const body = req.body
    const [user, league] = await Promise.all([
      this.useCase.findUserById(body.userId),
      this.useCase.findLeagueById(body.leagueId),
    ])
    if (!user) {
      return res.status(400).send({ message: 'UserId not found' })
    }
    if (!league) {
      return res.status(400).send({ message: 'leagueId not found' })
    }
    const payment: PaymentModel = {
      amount: body.amount,
      league,
      user,
    }
    const result = await this.useCase.insert(payment)
    res.json(result)
  }

  async remove(req: Request, res: Response) {
    const params = req.params
    const id = +params.id
    const result = await this.useCase.remove({ id })
    res.json(result)
  }
}
