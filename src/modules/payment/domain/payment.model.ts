import LeagueModel from '../../league/domain/league.model'
import UserModel from '../../user/domain/user.model'

export default interface PaymentModel {
  id?: number
  amount: number
  league: LeagueModel
  user: UserModel
  createAt?: Date
  updateAt?: Date
}
