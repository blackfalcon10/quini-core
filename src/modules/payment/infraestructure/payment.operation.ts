import { getRepository, Repository } from 'typeorm'
import League from '../../../entities/league.entity'
import Payment from '../../../entities/payment.entity'
import User from '../../../entities/user.entity'
import LeagueModel from '../../league/domain/league.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import UserModel from '../../user/domain/user.model'
import PaymentModel from '../domain/payment.model'

export default class PaymentOperation extends OperationBase<PaymentModel> {
  constructor() {
    super(Payment)
  }

  async findPaymentByKey(key: string, value: string): Promise<PaymentModel> {
    const repository: Repository<PaymentModel> = getRepository(Payment)
    const where = { [key]: value }
    return repository.findOne(where)
  }

  async findUserById(id: number): Promise<UserModel> {
    const repository: Repository<UserModel> = getRepository(User)
    return repository.findOne(id)
  }

  async findLeagueById(id: number): Promise<LeagueModel> {
    const repository: Repository<LeagueModel> = getRepository(League)
    return repository.findOne(id)
  }
}
