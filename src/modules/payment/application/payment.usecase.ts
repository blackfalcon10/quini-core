import LeagueModel from '../../league/domain/league.model'
import UseCaseBase from '../../shared/application/base.usecase'
import UserModel from '../../user/domain/user.model'
import PaymentModel from '../domain/payment.model'
import PaymentRepository from './payment.repository'

export default class PaymentUseCase extends UseCaseBase<
  PaymentModel,
  PaymentRepository
> {
  constructor(operation: PaymentRepository) {
    super(operation)
  }

  async findPaymentByKey(key: string, value: string): Promise<PaymentModel> {
    return this.operation.findPaymentByKey(key, value)
  }

  async findUserById(id: number): Promise<UserModel> {
    return this.operation.findUserById(id)
  }

  async findLeagueById(id: number): Promise<LeagueModel> {
    return this.operation.findLeagueById(id)
  }
}
