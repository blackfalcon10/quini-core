import LeagueModel from '../../league/domain/league.model'
import RepositoryBase from '../../shared/application/base.repository'
import UserModel from '../../user/domain/user.model'
import PaymentModel from '../domain/payment.model'

export default interface PaymentRepository
  extends RepositoryBase<PaymentModel> {
  findPaymentByKey(key: string, value: string): Promise<PaymentModel>
  findUserById(id: number): Promise<UserModel>
  findLeagueById(id: number): Promise<LeagueModel>
}
