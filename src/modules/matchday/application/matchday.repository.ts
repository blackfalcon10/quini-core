import ForecastModel from '../../forecast/domain/forecast.model'
import LeagueModel from '../../league/domain/league.model'
import MatchModel from '../../match/domain/match.model'
import RepositoryBase from '../../shared/application/base.repository'
import MatchdayModel from '../domain/matchday.model'

export default interface MatchdayRepository
  extends RepositoryBase<MatchdayModel> {
  findMatchdayByKeys(matchday: Partial<MatchdayModel>): Promise<MatchdayModel>
  findLeagueById(id: number): Promise<LeagueModel>
  findMatchesByMatchdayId(
    matchday: Partial<MatchdayModel>
  ): Promise<MatchModel[]>
  findForecastsByMatchdayId(
    matchday: Partial<MatchdayModel>
  ): Promise<ForecastModel[]>
}
