import ForecastModel from '../../forecast/domain/forecast.model'
import LeagueModel from '../../league/domain/league.model'
import MatchModel from '../../match/domain/match.model'
import UseCaseBase from '../../shared/application/base.usecase'
import MatchdayModel from '../domain/matchday.model'
import MatchdayRepository from './matchday.repository'

export default class MatchdayUseCase extends UseCaseBase<
  MatchdayModel,
  MatchdayRepository
> {
  constructor(operation: MatchdayRepository) {
    super(operation)
  }

  async findLeagueById(id: number): Promise<LeagueModel> {
    return this.operation.findLeagueById(id)
  }

  async findMatchdayByKeys(
    matchday: Partial<MatchdayModel>
  ): Promise<MatchdayModel> {
    return this.operation.findMatchdayByKeys(matchday)
  }

  async findMatchesByMatchdayId(
    matchday: Partial<MatchdayModel>
  ): Promise<MatchModel[]> {
    return this.operation.findMatchesByMatchdayId(matchday)
  }

  async getRobotByMatchId(
    matchday: Partial<MatchdayModel>
  ): Promise<MatchModel[]> {
    const forecasts = await this.operation.findForecastsByMatchdayId(matchday)
    let robots: { home: number; away: number; draw: number }[] = []
    forecasts.forEach(({ local, visitor, match }) => {
      let home = 0
      let away = 0
      let draw = 0
      if (robots[match.id]) {
        home = robots[match.id].home
        away = robots[match.id].away
        draw = robots[match.id].draw
      }

      if (local > visitor) {
        home += 1
      } else if (local < visitor) {
        away += 1
      } else if (local === visitor) {
        draw += 1
      }
      robots[match.id] = {
        home,
        away,
        draw,
      }
    })
    robots = robots.map(({ home, away, draw }) => {
      const total = home + away + draw
      const homePercent = (home * 100) / total
      const awayPercent = (away * 100) / total
      const drawPercent = (draw * 100) / total
      return {
        home: ((homePercent + 33) * 100) / 200,
        away: ((awayPercent + 33) * 100) / 200,
        draw: ((drawPercent + 34) * 100) / 200,
      }
    })
    const matches = await this.operation.findMatchesByMatchdayId(matchday)
    return matches.map((match) => {
      return {
        ...match,
        probability: {
          local: parseFloat(
            `${Math.round(robots[match.id].home * 100) / 100}`
          ).toFixed(2),
          visitor: parseFloat(
            `${Math.round(robots[match.id].away * 100) / 100}`
          ).toFixed(2),
          draw: parseFloat(
            `${Math.round(robots[match.id].draw * 100) / 100}`
          ).toFixed(2),
        },
      }
    })
  }
}
