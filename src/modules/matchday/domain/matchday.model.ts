import LeagueModel from '../../league/domain/league.model'

export default interface MatchdayModel {
  id?: number
  num: number
  totalGoals?: number
  status?: boolean
  league: LeagueModel
}
