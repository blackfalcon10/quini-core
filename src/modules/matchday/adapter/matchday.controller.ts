import MatchdayUseCase from '../application/matchday.usecase'
import { Request, Response } from 'express'
import MatchdayModel from '../domain/matchday.model'

export default class MatchdayController {
  constructor(private useCase: MatchdayUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list({}, ['league'])
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const matchday: Partial<MatchdayModel> = { id }
    const result = await this.useCase.listOne(matchday, ['league'])
    res.json(result)
  }

  async listMatches(req: Request, res: Response) {
    const id = +req.params.id
    const matchday: Partial<MatchdayModel> = { id }
    const result = await this.useCase.findMatchesByMatchdayId(matchday)
    res.json(result)
  }

  async getRobot(req: Request, res: Response) {
    const id = +req.params.id
    const matchday: Partial<MatchdayModel> = { id }
    const result = await this.useCase.getRobotByMatchId(matchday)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    const body = req.body
    const league = await this.useCase.findLeagueById(body.leagueId)
    if (!league) {
      return res.status(400).send({ message: 'LeagueId not found' })
    }
    const matchdayAlreadyExist = await this.useCase.findMatchdayByKeys({
      num: body.num,
      league: league,
    })
    if (matchdayAlreadyExist) {
      return res.status(400).send({
        message: `Matchday num already exists with leagueId ${league.id}`,
      })
    }
    const matchday: MatchdayModel = {
      num: body.num,
      totalGoals: body.totalGoals,
      status: body.status,
      league: league,
    }
    const result = await this.useCase.insert(matchday)
    res.json(result)
  }

  async update(req: Request, res: Response) {
    const id = req.params.id
    const body = req.body
    const league = await this.useCase.findLeagueById(body.leagueId)
    if (!league) {
      return res.status(400).send({ message: 'LeagueId not found' })
    }
    const validateId = await this.useCase.findMatchdayByKeys({ id: +id })
    if (!validateId) {
      return res.status(400).send({ message: 'Id not found' })
    }
    if (validateId.id !== body.num) {
      const matchdayAlreadyExist = await this.useCase.findMatchdayByKeys({
        num: body.num,
        league: league,
      })
      if (matchdayAlreadyExist) {
        return res.status(400).send({
          message: `Matchday num already exists with leagueId ${league.id}`,
        })
      }
    }
    const matchday: MatchdayModel = {
      num: body.num,
      totalGoals: body.totalGoals,
      status: body.status,
      league: league,
    }
    const result = await this.useCase.update(matchday, { id })
    res.json(result)
  }

  async remove(req: Request, res: Response) {
    const params = req.params
    const id = +params.id
    const result = await this.useCase.remove({ id })
    res.json(result)
  }
}
