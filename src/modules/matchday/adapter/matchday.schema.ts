import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      num: Joi.number().required(),
      totalGoals: Joi.number(),
      status: Joi.boolean(),
      leagueId: Joi.number().required(),
      auth: Joi.object().required(),
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      num: Joi.number(),
      totalGoals: Joi.number(),
      status: Joi.boolean(),
      leagueId: Joi.number(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
