import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import MatchdayUseCase from '../application/matchday.usecase'
import MatchdayOperation from '../infraestructure/matchday.operation'
import MatchdayController from './matchday.controller'
import MatchdaySchema from './matchday.schema'

const operation = new MatchdayOperation()
const useCase = new MatchdayUseCase(operation)
const controller = new MatchdayController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.get(
  '/:id/matches',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listMatches.bind(controller))
)
route.get(
  '/:id/robot',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.LIST_ONE),
  ErrorHandler.asyncError(controller.getRobot.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
route.put(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.UPDATE.body),
  controller.update.bind(controller)
)
route.delete(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(MatchdaySchema.REMOVE.params),
  ErrorHandler.asyncError(controller.remove.bind(controller))
)

export { route }
