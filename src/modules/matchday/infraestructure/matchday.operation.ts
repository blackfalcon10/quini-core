import { getRepository, Repository, In } from 'typeorm'
import Matchday from '../../../entities/matchday.entity'
import League from '../../../entities/league.entity'
import LeagueModel from '../../league/domain/league.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import MatchdayModel from '../domain/matchday.model'
import MatchModel from '../../match/domain/match.model'
import Match from '../../../entities/match.entity'
import TeamModel from '../../team/domain/team.model'
import Team from '../../../entities/team.entity'
import ForecastModel from '../../forecast/domain/forecast.model'
import Forecast from '../../../entities/forecast.entity'

export default class MatchdayOperation extends OperationBase<MatchdayModel> {
  constructor() {
    super(Matchday)
  }

  async findMatchdayByKeys(matchday: MatchdayModel): Promise<MatchdayModel> {
    const repository: Repository<MatchdayModel> = getRepository(Matchday)
    const where = matchday
    return repository.findOne(where)
  }

  async findLeagueById(id: number): Promise<LeagueModel> {
    const repository: Repository<LeagueModel> = getRepository(League)
    return repository.findOne(id)
  }

  async findMatchesByMatchdayId(
    matchday: Partial<MatchdayModel>
  ): Promise<MatchModel[]> {
    const repository: Repository<MatchModel> = getRepository(Match)
    const resultMatches = await repository.find({ matchday: matchday })
    const teams = await this.getTeamsByMatches(resultMatches)
    return this.mapperMatchesWithTeam(resultMatches, teams)
  }

  async getTeamsByMatches(resultMatches: MatchModel[]) {
    const teamsIdsData: any[] = []
    resultMatches.forEach(({ local, visitor }) => {
      teamsIdsData.push(local.teamId, visitor.teamId)
    })
    const removeTeamIdsRepeat = new Set(teamsIdsData)
    const teamsIds: number[] = [...removeTeamIdsRepeat]
    const repositoryTeam: Repository<TeamModel> = getRepository(Team)
    return repositoryTeam.find({ id: In(teamsIds) })
  }

  mapperMatchesWithTeam(resultMatches: MatchModel[], teams: TeamModel[]) {
    return resultMatches.map((match) => {
      return {
        ...match,
        local: {
          teamId: teams.find(({ id }) => id === Number(match.local.teamId)),
          result: match.local.result,
        },
        visitor: {
          teamId: teams.find(({ id }) => id === Number(match.visitor.teamId)),
          result: match.visitor.result,
        },
      }
    })
  }

  async findForecastsByMatchdayId(
    matchday: Partial<MatchdayModel>
  ): Promise<ForecastModel[]> {
    const repository: Repository<ForecastModel> = getRepository(Forecast)
    return repository.find({
      where: { matchday },
      select: ['local', 'visitor'],
      relations: ['match'],
      order: {
        match: 'ASC',
      },
    })
  }
}
