import _ from 'lodash'
import MatchdayModel from '../../matchday/domain/matchday.model'
import UseCaseBase from '../../shared/application/base.usecase'
import UserModel from '../../user/domain/user.model'
import ScoreModel from '../domain/score.model'
import ScoreRepository from './score.repository'

export default class ScoreUseCase extends UseCaseBase<
  ScoreModel,
  ScoreRepository
> {
  constructor(operation: ScoreRepository) {
    super(operation)
  }
  async findScoreByKey(forecast: Partial<ScoreModel>): Promise<ScoreModel> {
    return this.operation.findScoreByKey(forecast)
  }
  async findUserByIds(ids: number[]): Promise<UserModel[]> {
    return this.operation.findUserByIds(ids)
  }
  async findMatchdayById(id: number): Promise<MatchdayModel> {
    return this.operation.findMatchdayById(id)
  }
  async insertMany(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    return this.operation.insertMany(scores)
  }
  async updateMany(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    return this.operation.updateMany(scores)
  }

  async updateOrCreateScores({
    matchdayId,
    scores,
  }: {
    matchdayId: number
    scores: any[]
  }): Promise<ScoreModel[]> {
    const [users, matchday] = await Promise.all([
      this.findUserByIds(
        scores.map(({ userId }: { userId: number }) => userId)
      ),
      this.findMatchdayById(matchdayId),
    ])
    let position = 1
    const dataSave: Partial<ScoreModel>[] = []
    const dataUpdate: Partial<ScoreModel>[] = []
    const scoresOrderBy = _.orderBy(scores, ['win', 'score'], ['desc', 'desc'])

    await Promise.all(
      scoresOrderBy.map(async (score) => {
        const user = users.find(({ id }) => id === score.userId)
        if (user) {
          const existScore = await this.findScoreByKey({
            user: user,
            matchday: matchday,
          })
          if (existScore) {
            dataUpdate.push({
              id: existScore.id,
              ...score,
              user,
              matchday,
              position,
            })
          } else {
            dataSave.push({
              ...score,
              user,
              matchday,
              position,
            })
          }
          position++
        }
      })
    )
    const [save, update] = await Promise.all([
      this.insertMany(dataSave),
      this.updateMany(dataUpdate),
    ])
    return _.merge(save, update)
  }
}
