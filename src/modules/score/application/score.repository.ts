import MatchdayModel from '../../matchday/domain/matchday.model'
import RepositoryBase from '../../shared/application/base.repository'
import UserModel from '../../user/domain/user.model'
import ScoreModel from '../domain/score.model'

export default interface ScoreRepository extends RepositoryBase<ScoreModel> {
  findScoreByKey(forecast: Partial<ScoreModel>): Promise<ScoreModel>
  findUserByIds(id: number[]): Promise<UserModel[]>
  findMatchdayById(id: number): Promise<MatchdayModel>
  insertMany(forecasts: Partial<ScoreModel>[]): Promise<ScoreModel[]>
  updateMany(forecasts: Partial<ScoreModel>[]): Promise<ScoreModel[]>
}
