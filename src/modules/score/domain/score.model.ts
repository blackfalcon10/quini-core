import MatchdayModel from '../../matchday/domain/matchday.model'
import UserModel from '../../user/domain/user.model'

export default interface ScoreModel {
  id?: number
  position: number
  win: number
  lost: number
  score: number
  user: Partial<UserModel>
  matchday: Partial<MatchdayModel>
}
