import _ from 'lodash'
import { getRepository, Repository, In } from 'typeorm'
import Matchday from '../../../entities/matchday.entity'
import Score from '../../../entities/score.entity'
import User from '../../../entities/user.entity'
import MatchdayModel from '../../matchday/domain/matchday.model'
import OperationBase from '../../shared/infraestructure/base.operation'
import UserModel from '../../user/domain/user.model'
import ScoreModel from '../domain/score.model'

export default class ScoreOperation extends OperationBase<ScoreModel> {
  constructor() {
    super(Score)
  }

  async findScoreByKey(score: ScoreModel): Promise<ScoreModel> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    const where = score
    return repository.findOne(where)
  }

  async findUserByIds(ids: number[]): Promise<UserModel[]> {
    const repository: Repository<UserModel> = getRepository(User)
    return repository.find({
      id: In(ids),
    })
  }
  async findMatchdayById(id: number): Promise<MatchdayModel> {
    const repository: Repository<MatchdayModel> = getRepository(Matchday)
    return repository.findOne(id)
  }

  async insertMany(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    return repository.save(scores)
  }

  async updateMany(scores: Partial<ScoreModel>[]): Promise<ScoreModel[]> {
    const repository: Repository<ScoreModel> = getRepository(Score)
    let recordToUpdate = await repository.find({
      id: In(scores.map(({ id }) => id)),
    })
    recordToUpdate = _.merge(recordToUpdate, scores)

    return repository.save(recordToUpdate)
  }
}
