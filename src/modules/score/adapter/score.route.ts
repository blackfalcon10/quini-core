import express from 'express'
import ErrorHandler from '../../../helpers/error.helper'
import { mergeParameters } from '../../../helpers/parameters.helper'
import { validateParameters } from '../../../helpers/validate.helper'
import { AuthenticationGuard } from '../../shared/guards/authentication.guard'
import ScoreUseCase from '../application/score.usecase'
import ScoreOperation from '../infraestructure/score.operation'
import ScoreController from './score.controller'
import ScoreSchema from './score.schema'

const operation = new ScoreOperation()
const useCase = new ScoreUseCase(operation)
const controller = new ScoreController(useCase)
const route = express.Router()

route.get(
  '/',
  AuthenticationGuard.canActivate,
  ErrorHandler.asyncError(controller.list.bind(controller))
)
route.get(
  '/:id',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ScoreSchema.LIST_ONE),
  ErrorHandler.asyncError(controller.listOne.bind(controller))
)
route.post(
  '/',
  AuthenticationGuard.canActivate,
  mergeParameters,
  validateParameters(ScoreSchema.INSERT.body),
  ErrorHandler.asyncError(controller.insert.bind(controller))
)
export { route }
