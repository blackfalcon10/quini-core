import Joi from 'joi'

const paramId = Joi.object({
  id: Joi.number().required(),
  auth: Joi.object().required(),
})

const score = Joi.object().keys({
  win: Joi.number().required(),
  lost: Joi.number().required(),
  score: Joi.number().required(),
  userId: Joi.number().required(),
  auth: Joi.object().required(),
})

const schemas = {
  LIST_ONE: paramId,
  INSERT: {
    body: Joi.object({
      matchdayId: Joi.number().required(),
      scores: Joi.array().items(score).required(),
      auth: Joi.object().required(),
    }),
  },
  UPDATE: {
    body: Joi.object({
      id: Joi.number().required(),
      matchdayId: Joi.number().required(),
      scores: Joi.array().items(score).required(),
      auth: Joi.object().required(),
    }),
  },
  REMOVE: {
    params: paramId,
  },
}

export default schemas
