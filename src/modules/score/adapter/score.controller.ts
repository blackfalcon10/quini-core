import ScoreUseCase from '../application/score.usecase'
import { Request, Response } from 'express'
import ScoreModel from '../domain/score.model'

export default class ScoreController {
  constructor(private useCase: ScoreUseCase) {
    this.useCase = useCase
  }

  async list(req: Request, res: Response) {
    const result = await this.useCase.list()
    res.json(result)
  }

  async listOne(req: Request, res: Response) {
    const id = +req.params.id
    const score: Partial<ScoreModel> = { id }
    const result = await this.useCase.listOne(score)
    res.json(result)
  }

  async insert(req: Request, res: Response) {
    res.json({
      status: true,
      msg: 'Los resultados de la quiniela se ha guardado correctamente.',
      scores: await this.useCase.updateOrCreateScores(req.body),
    })
  }
}
