import ServerBootstrap from './bootstrap/server.bootstrap'
import Logger from './helpers/logger.helper'
import DatabaseBootstrap from './bootstrap/database.bootstrap'

import app from './app'
;(async () => {
  const serverBootstrap = new ServerBootstrap(app)
  const databaseBootstrap = new DatabaseBootstrap()

  try {
    await serverBootstrap.initialize()
    await databaseBootstrap.initialize()
  } catch (error) {
    Logger.error(error)
    databaseBootstrap.getConnection().close()
    process.exit(1)
  }
})()
