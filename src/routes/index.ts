import express from 'express'
import { route as RouteUser } from '../modules/user/adapter/user.route'
import { route as RouteTeam } from '../modules/team/adapter/team.route'
import { route as RouteLeague } from '../modules/league/adapter/league.route'
import { route as RouteMatchday } from '../modules/matchday/adapter/matchday.route'
import { route as RouteMatch } from '../modules/match/adapter/match.route'
import { route as RouteForecast } from '../modules/forecast/adapter/forecast.route'
import { route as RouteScore } from '../modules/score/adapter/score.route'
import { route as RoutePayment } from '../modules/payment/adapter/payment.route'
import { route as RouteAuth } from '../modules/auth/adapter/auth.route'

const router = express.Router()

router.use('/users', RouteUser)
router.use('/teams', RouteTeam)
router.use('/leagues', RouteLeague)
router.use('/matchdays', RouteMatchday)
router.use('/matchs', RouteMatch)
router.use('/forecasts', RouteForecast)
router.use('/scores', RouteScore)
router.use('/payments', RoutePayment)
router.use('/auth', RouteAuth)

export default router
