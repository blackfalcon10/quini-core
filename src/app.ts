import express, { Request, Response } from 'express'
import helmet from 'helmet'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import expressUseragent from 'express-useragent'
import routerApi from './routes'

import ErrorHandler from './helpers/error.helper'

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(helmet())
app.use(cors())
app.use(expressUseragent.express())

app.use('/api/v1', routerApi)

app.get('/health', (req: Request, res: Response) => res.send('I am alive'))

app.use(ErrorHandler.notFound)
app.use(ErrorHandler.generic)

export default app
