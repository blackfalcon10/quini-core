import { NextFunction, Request, Response } from 'express'

export const mergeParameters = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let parameters = {
    auth: res.locals.auth,
  }
  if (Object.prototype.hasOwnProperty.call(req, 'params')) {
    parameters = { ...req.params, ...parameters }
  }

  if (Object.prototype.hasOwnProperty.call(req, 'query')) {
    parameters = { ...req.query, ...parameters }
  }

  // if (Object.prototype.hasOwnProperty.call(req, 'headers')) {
  //   parameters = { ...req.headers, ...parameters };
  // }

  if (Object.prototype.hasOwnProperty.call(req, 'body')) {
    parameters = { ...req.body, ...parameters }
  }

  res.locals = parameters

  next()
}
