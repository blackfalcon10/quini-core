import DatabaseBootstrap from '../../src/bootstrap/database.bootstrap'
import request from 'supertest'
import app from '../../src/app'
import { makeFakeNewLeague } from '../mocks/league.mock'
import sinon from 'sinon'
import leagueOperation from '../../src/modules/league/infraestructure/league.operation'

const databaseBootstrap = new DatabaseBootstrap()

const sandbox = sinon.createSandbox()

describe('leagues', () => {
  beforeAll(async () => {
    await databaseBootstrap.initialize()
  })

  beforeEach(async () => {
    await databaseBootstrap.cleanDb()
  })

  afterEach(async () => {
    sandbox.restore()
  })

  afterAll(async () => {
    await databaseBootstrap.getConnection().close()
  })

  it('GET leagues empty values', async () => {
    const response = await request(app).get('/api/v1/leagues').expect(200)

    expect(response.body.payload).toEqual({ data: [] })
  })

  it('GET leagues with values', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { body: get } = await request(app)
      .get(`/api/v1/leagues/${post.payload.data.id}`)
      .expect(200)

    expect(get).toEqual(post)
  })

  it('GET leagues error handler', async () => {
    sandbox
      .stub(leagueOperation.prototype, 'list')
      .rejects(new Error('Test error'))

    const { body: get } = await request(app).get('/api/v1/leagues').expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('GET leagues/:id with with values', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { data } = post.payload

    const { body: get } = await request(app)
      .get(`/api/v1/leagues/${data.id}`)
      .expect(200)

    expect(get.payload).toEqual({
      data,
    })
  })

  it('GET leagues/:id with empty values', async () => {
    const leagueId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/leagues/${leagueId}`)
      .expect(200)

    expect(get).toEqual({
      payload: {},
    })
  })

  it('GET leagues/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .get(`/api/v1/leagues/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('GET leagues/:id error handler', async () => {
    sandbox
      .stub(leagueOperation.prototype, 'listOne')
      .rejects(new Error('Test error'))

    const leagueId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/leagues/${leagueId}`)
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('POST leagues', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { name, status, id } = post.payload.data

    expect(name).toBe(league.name)
    expect(status).toBe(league.status)
    expect(id).toBeTruthy()
  })

  it('POST leagues name repeat', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { name, status, isFree, id } = post.payload.data

    expect(name).toBe(league.name)
    expect(status).toBe(league.status)
    expect(isFree).toBe(league.isFree)
    expect(id).toBeTruthy()

    const { body: postRepeat } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(400)

    const { message } = postRepeat

    expect(message).toBe('League already exists')
  })

  it('POST leagues validations', async () => {
    const league = makeFakeNewLeague()
    delete league.name
    const { body } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(body).toEqual(["'name' is required"])
  })

  it('POST leagues error handler', async () => {
    const league = makeFakeNewLeague()
    sandbox
      .stub(leagueOperation.prototype, 'insert')
      .rejects(new Error('Test error'))

    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(500)

    const { name, status, message } = post

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('UPDATE leagues/:id league', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const nameUpdate = 'Soccer League'
    const { body: put } = await request(app)
      .put(`/api/v1/leagues/${post.payload.data.id}`)
      .send({ name: nameUpdate })
      .expect(200)

    const { name, status, id } = put.payload.data

    expect(name).toBe(nameUpdate)
    expect(status).toBe(league.status)
    expect(id).toBeTruthy()
  })

  it('UPDATE leagues/:id league repeat', async () => {
    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const {
      name: firstName,
      status: firstStatus,
      id: firstId,
    } = post.payload.data

    expect(firstName).toBe(league.name)
    expect(firstStatus).toBe(league.status)
    expect(firstId).toBeTruthy()

    const leagueRepeat = { name: 'Soccer League' }
    const { body: postRepeat } = await request(app)
      .post('/api/v1/leagues/')
      .send(leagueRepeat)
      .expect(200)
    const { id: idRepeat } = postRepeat.payload.data

    const { body: put } = await request(app)
      .put(`/api/v1/leagues/${idRepeat}`)
      .send(league)
      .expect(400)

    const { message } = put

    expect(message).toBe('League already exists')
  })

  it('UPDATE leagues/:id validation of the id', async () => {
    const league = makeFakeNewLeague()
    const invalidId = 5
    const { body: put } = await request(app)
      .put(`/api/v1/leagues/${invalidId}`)
      .send(league)
      .expect(400)

    const { message } = put

    expect(message).toEqual('Id not found')
  })

  it('DELETE league/:id', async () => {
    const league = makeFakeNewLeague()
    const { body } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { id } = body.payload.data

    const { body: get } = await request(app)
      .del(`/api/v1/leagues/${id}`)
      .expect(200)

    expect(get).toEqual({
      payload: {
        data: {
          affected: 1,
          raw: [],
        },
      },
    })
  })

  it('DELETE leagues/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .del(`/api/v1/leagues/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('DELETE leagues/:id error handler', async () => {
    sandbox
      .stub(leagueOperation.prototype, 'remove')
      .rejects(new Error('Test error'))

    const league = makeFakeNewLeague()
    const { body: post } = await request(app)
      .post('/api/v1/leagues/')
      .send(league)
      .expect(200)

    const { body: del } = await request(app)
      .del(`/api/v1/leagues/${post.payload.data.id}`)
      .expect(500)

    const { name, status, message } = del

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })
})
