import DatabaseBootstrap from '../../src/bootstrap/database.bootstrap'
import request from 'supertest'
import app from '../../src/app'
import { makeFakeNewMatchday } from '../mocks/matchday.mock'
import sinon from 'sinon'
import matchdayOperation from '../../src/modules/matchday/infraestructure/matchday.operation'
import { makeFakeNewLeague } from '../mocks/league.mock'

const databaseBootstrap = new DatabaseBootstrap()

const sandbox = sinon.createSandbox()

describe('matchdays', () => {
  beforeAll(async () => {
    await databaseBootstrap.initialize()
  })

  beforeEach(async () => {
    await databaseBootstrap.cleanDb()
  })

  afterEach(async () => {
    sandbox.restore()
  })

  afterAll(async () => {
    await databaseBootstrap.getConnection().close()
  })

  it('GET matchdays empty values', async () => {
    const response = await request(app).get('/api/v1/matchdays').expect(200)

    expect(response.body.payload).toEqual({ data: [] })
  })

  it('GET matchdays with values', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const { body: get } = await request(app)
      .get('/api/v1/matchdays')
      .expect(200)

    expect(get.payload).toEqual({ data: [post.payload.data] })
  })

  it('GET matchdays error handler', async () => {
    sandbox
      .stub(matchdayOperation.prototype, 'list')
      .rejects(new Error('Test error'))

    const { body: get } = await request(app)
      .get('/api/v1/matchdays')
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('GET matchdays/:id with with values', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const { data } = post.payload

    const { body: get } = await request(app)
      .get(`/api/v1/matchdays/${data.id}`)
      .expect(200)

    expect(get.payload).toEqual({
      data,
    })
  })

  it('GET matchdays/:id with empty values', async () => {
    const matchdayId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/matchdays/${matchdayId}`)
      .expect(200)

    expect(get).toEqual({
      payload: {},
    })
  })

  it('GET matchdays/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .get(`/api/v1/matchdays/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('GET matchdays/:id error handler', async () => {
    sandbox
      .stub(matchdayOperation.prototype, 'listOne')
      .rejects(new Error('Test error'))

    const matchdayId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/matchdays/${matchdayId}`)
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('POST matchdays', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const { num, status, id } = post.payload.data

    expect(num).toBe(matchday.num)
    expect(status).toBe(matchday.status)
    expect(id).toBeTruthy()
  })

  it('POST matchdays num and league repeat', async () => {
    const leagueFake = makeFakeNewLeague()
    const { body: league } = await request(app)
      .post('/api/v1/leagues/')
      .send(leagueFake)

    const leagueId = league.payload.data.id

    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId,
      })
      .expect(200)

    const { num, status, id } = post.payload.data
    expect(num).toBe(matchday.num)
    expect(status).toBe(matchday.status)
    expect(id).toBeTruthy()

    const { body: postRepeat } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num,
        leagueId,
      })
      .expect(400)

    const { message } = postRepeat
    expect(message).toBe(
      `Matchday num already exists with leagueId ${leagueId}`
    )
  })

  it('POST matchdays validations', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    delete matchday.num
    const { body } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(400)

    // eslint-disable-next-line quotes
    expect(body).toEqual(["'num' is required"])
  })

  it('POST matchdays error handler', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    sandbox
      .stub(matchdayOperation.prototype, 'insert')
      .rejects(new Error('Test error'))

    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(500)

    const { name, status, message } = post

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('UPDATE matchdays/:id matchday', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const totalGoalsUpdate = 10
    const { body: put } = await request(app)
      .put(`/api/v1/matchdays/${post.payload.data.id}`)
      .send({ totalGoals: totalGoalsUpdate })
      .expect(200)

    const { num, status, id, totalGoals } = put.payload.data
    expect(num).toBe(matchday.num)
    expect(totalGoals).toBe(totalGoalsUpdate)
    expect(status).toBe(matchday.status)
    expect(id).toBeTruthy()
  })

  it('UPDATE matchdays/:id matchday num and league repeat', async () => {
    const leagueFake = makeFakeNewLeague()
    const { body: league } = await request(app)
      .post('/api/v1/leagues/')
      .send(leagueFake)
    const leagueId = league.payload.data.id
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId,
      })
      .expect(200)

    const {
      num: firstName,
      status: firstStatus,
      id: firstId,
    } = post.payload.data

    expect(firstName).toBe(matchday.num)
    expect(firstStatus).toBe(matchday.status)
    expect(firstId).toBeTruthy()

    const { body: postRepeat } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num + 1,
        leagueId,
      })
      .expect(200)
    const { id: idRepeat } = postRepeat.payload.data

    const { body: put } = await request(app)
      .put(`/api/v1/matchdays/${idRepeat}`)
      .send({
        num: matchday.num,
      })
      .expect(400)

    const { message } = put

    expect(message).toBe(
      `Matchday num already exists with leagueId ${leagueId}`
    )
  })

  it('UPDATE matchdays/:id validation of the id', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const invalidId = 5
    const { body: put } = await request(app)
      .put(`/api/v1/matchdays/${invalidId}`)
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(400)

    const { message } = put

    expect(message).toEqual('Id not found')
  })

  it('DELETE matchday/:id', async () => {
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const { id } = body.payload.data

    const { body: get } = await request(app)
      .del(`/api/v1/matchdays/${id}`)
      .expect(200)

    expect(get).toEqual({
      payload: {
        data: {
          affected: 1,
          raw: [],
        },
      },
    })
  })

  it('DELETE matchdays/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .del(`/api/v1/matchdays/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('DELETE matchdays/:id error handler', async () => {
    sandbox
      .stub(matchdayOperation.prototype, 'remove')
      .rejects(new Error('Test error'))

    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const matchday = makeFakeNewMatchday()
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchday.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    const { body: del } = await request(app)
      .del(`/api/v1/matchdays/${post.payload.data.id}`)
      .expect(500)

    const { name, status, message } = del

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })
})
