import DatabaseBootstrap from '../../src/bootstrap/database.bootstrap'
import request from 'supertest'
import app from '../../src/app'
import { makeFakeNewMatch } from '../mocks/match.mock'
import sinon from 'sinon'
import matchOperation from '../../src/modules/match/infraestructure/match.operation'
import { makeFakeNewLeague } from '../mocks/league.mock'
import { makeFakeNewMatchday } from '../mocks/matchday.mock'
import MatchdayModel from '../../src/modules/matchday/domain/matchday.model'
import moment from 'moment'

const databaseBootstrap = new DatabaseBootstrap()

const sandbox = sinon.createSandbox()
let matchdayFake: MatchdayModel = makeFakeNewMatchday()

describe('matchs', () => {
  beforeAll(async () => {
    await databaseBootstrap.initialize()
  })

  beforeEach(async () => {
    sandbox.restore()
    await databaseBootstrap.cleanDb()
    const leagueFake = makeFakeNewLeague()
    const league = await request(app).post('/api/v1/leagues/').send(leagueFake)
    const { body: post } = await request(app)
      .post('/api/v1/matchdays/')
      .send({
        num: matchdayFake.num,
        leagueId: league.body.payload.data.id,
      })
      .expect(200)

    matchdayFake = post.payload.data
  })

  afterAll(async () => {
    await databaseBootstrap.getConnection().close()
  })

  it('GET matchs empty values', async () => {
    const response = await request(app).get('/api/v1/matchs').expect(200)

    expect(response.body.payload).toEqual({ data: [] })
  })

  it('GET matchs with values', async () => {
    const match = makeFakeNewMatch()
    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const { body: get } = await request(app)
      .get(`/api/v1/matchs/${post.payload.data.id}`)
      .expect(200)

    expect(get).toEqual(post)
  })

  it('GET matchs error handler', async () => {
    sandbox
      .stub(matchOperation.prototype, 'list')
      .rejects(new Error('Test error'))

    const { body: get } = await request(app).get('/api/v1/matchs').expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('GET matchs/:id with with values', async () => {
    const match = makeFakeNewMatch()
    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const { data } = post.payload

    const { body: get } = await request(app)
      .get(`/api/v1/matchs/${data.id}`)
      .expect(200)

    expect(get.payload).toEqual({
      data,
    })
  })

  it('GET matchs/:id with empty values', async () => {
    const matchId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/matchs/${matchId}`)
      .expect(200)

    expect(get).toEqual({
      payload: {},
    })
  })

  it('GET matchs/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .get(`/api/v1/matchs/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('GET matchs/:id error handler', async () => {
    sandbox
      .stub(matchOperation.prototype, 'listOne')
      .rejects(new Error('Test error'))

    const matchId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/matchs/${matchId}`)
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('POST matchs', async () => {
    const match = makeFakeNewMatch()
    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const { local, visitor, endDate, status, matchday, id } = post.payload.data

    expect(local).toMatchObject(match.local)
    expect(visitor).toMatchObject(match.visitor)
    expect(moment(endDate).toString()).toBe(moment(match.endDate).toString())
    expect(status).toBe(match.status)
    expect(matchday).toMatchObject(matchdayFake)
    expect(id).toBeTruthy()
  })

  it('POST matchs validations', async () => {
    const match = makeFakeNewMatch()
    delete match.local
    const { body } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(400)

    // eslint-disable-next-line quotes
    expect(body).toEqual(["'local' is required"])
  })

  it('POST matchs error handler', async () => {
    const match = makeFakeNewMatch()
    sandbox
      .stub(matchOperation.prototype, 'insert')
      .rejects(new Error('Test error'))

    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(500)

    const { name, status, message } = post

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('UPDATE matchs/:id match', async () => {
    const match = makeFakeNewMatch()
    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const matchSave = post.payload.data
    const matchEdit = {
      local: {
        teamId: matchSave.local.teamId,
        result: 1,
      },
      visitor: {
        teamId: matchSave.visitor.teamId,
        result: 0,
      },
      status: true,
    }
    const { body: put } = await request(app)
      .put(`/api/v1/matchs/${matchSave.id}`)
      .send(matchEdit)
      .expect(200)

    const { local, visitor, status, id } = put.payload.data

    expect(local).toMatchObject(matchEdit.local)
    expect(visitor).toMatchObject(matchEdit.visitor)
    expect(status).toBe(matchEdit.status)
    expect(id).toBeTruthy()
  })

  it('UPDATE matchs/:id validation of the id', async () => {
    const match = makeFakeNewMatch()
    const invalidId = 5
    const { body: put } = await request(app)
      .put(`/api/v1/matchs/${invalidId}`)
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(400)

    const { message } = put

    expect(message).toEqual('Id not found')
  })

  it('DELETE match/:id', async () => {
    const match = makeFakeNewMatch()
    const { body } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const { id } = body.payload.data

    const { body: get } = await request(app)
      .del(`/api/v1/matchs/${id}`)
      .expect(200)

    expect(get).toEqual({
      payload: {
        data: {
          affected: 1,
          raw: [],
        },
      },
    })
  })

  it('DELETE matchs/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .del(`/api/v1/matchs/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('DELETE matchs/:id error handler', async () => {
    sandbox
      .stub(matchOperation.prototype, 'remove')
      .rejects(new Error('Test error'))

    const match = makeFakeNewMatch()
    const { body: post } = await request(app)
      .post('/api/v1/matchs/')
      .send({
        local: match.local,
        visitor: match.visitor,
        endDate: match.endDate,
        matchdayId: matchdayFake.id,
      })
      .expect(200)

    const { body: del } = await request(app)
      .del(`/api/v1/matchs/${post.payload.data.id}`)
      .expect(500)

    const { name, status, message } = del

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })
})
