import DatabaseBootstrap from '../../src/bootstrap/database.bootstrap'
import request from 'supertest'
import app from '../../src/app'
import { makeFakeNewTeam } from '../mocks/team.mock'
import sinon from 'sinon'
import teamOperation from '../../src/modules/team/infraestructure/team.operation'

const databaseBootstrap = new DatabaseBootstrap()

const sandbox = sinon.createSandbox()

describe('teams', () => {
  beforeAll(async () => {
    await databaseBootstrap.initialize()
  })

  beforeEach(async () => {
    await databaseBootstrap.cleanDb()
  })

  afterEach(async () => {
    sandbox.restore()
  })

  afterAll(async () => {
    await databaseBootstrap.getConnection().close()
  })

  it('GET teams empty values', async () => {
    const response = await request(app).get('/api/v1/teams').expect(200)

    expect(response.body.payload).toEqual({ data: [] })
  })

  it('GET teams with values', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { body: get } = await request(app).get('/api/v1/teams').expect(200)

    expect(get.payload).toEqual({ data: [post.payload.data] })
  })

  it('GET teams error handler', async () => {
    sandbox
      .stub(teamOperation.prototype, 'list')
      .rejects(new Error('Test error'))

    const { body: get } = await request(app).get('/api/v1/teams').expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('GET teams/:id with with values', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { data } = post.payload

    const { body: get } = await request(app)
      .get(`/api/v1/teams/${data.id}`)
      .expect(200)

    expect(get.payload).toEqual({
      data,
    })
  })

  it('GET teams/:id with empty values', async () => {
    const teamId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/teams/${teamId}`)
      .expect(200)

    expect(get).toEqual({
      payload: {},
    })
  })

  it('GET teams/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .get(`/api/v1/teams/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('GET teams/:id error handler', async () => {
    sandbox
      .stub(teamOperation.prototype, 'listOne')
      .rejects(new Error('Test error'))

    const teamId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/teams/${teamId}`)
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('POST teams', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { name, shield, id } = post.payload.data

    expect(name).toBe(team.name)
    expect(shield).toBe(team.shield)
    expect(id).toBeTruthy()
  })

  it('POST teams name repeat', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { name, shield, id } = post.payload.data

    expect(name).toBe(team.name)
    expect(shield).toBe(team.shield)
    expect(id).toBeTruthy()

    const { body: postRepeat } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(400)

    const { message } = postRepeat

    expect(message).toBe('Team already exists')
  })

  it('POST teams validations', async () => {
    const team = makeFakeNewTeam()
    delete team.name
    const { body } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(body).toEqual(["'name' is required"])
  })

  it('POST teams error handler', async () => {
    const team = makeFakeNewTeam()
    sandbox
      .stub(teamOperation.prototype, 'insert')
      .rejects(new Error('Test error'))

    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(500)

    const { name, status, message } = post

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('UPDATE teams/:id team', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const nameUpdate = 'America'
    const { body: put } = await request(app)
      .put(`/api/v1/teams/${post.payload.data.id}`)
      .send({ name: nameUpdate })
      .expect(200)

    const { name, shield, id } = put.payload.data

    expect(name).toBe(nameUpdate)
    expect(shield).toBe(team.shield)
    expect(id).toBeTruthy()
  })

  it('UPDATE teams/:id team repeat', async () => {
    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const {
      name: firstName,
      shield: firstShield,
      id: firstId,
    } = post.payload.data

    expect(firstName).toBe(team.name)
    expect(firstShield).toBe(team.shield)
    expect(firstId).toBeTruthy()

    const teamRepeat = { name: 'Puebla', shield: 'puebla.jpg' }
    const { body: postRepeat } = await request(app)
      .post('/api/v1/teams/')
      .send(teamRepeat)
      .expect(200)
    const { id: idRepeat } = postRepeat.payload.data

    const { body: put } = await request(app)
      .put(`/api/v1/teams/${idRepeat}`)
      .send(team)
      .expect(400)

    const { message } = put

    expect(message).toBe('Team already exists')
  })

  it('UPDATE teams/:id validation of the id', async () => {
    const team = makeFakeNewTeam()
    const invalidId = 5
    const { body: put } = await request(app)
      .put(`/api/v1/teams/${invalidId}`)
      .send(team)
      .expect(400)

    const { message } = put

    expect(message).toEqual('Id not found')
  })

  it('DELETE team/:id', async () => {
    const team = makeFakeNewTeam()
    const { body } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { id } = body.payload.data

    const { body: get } = await request(app)
      .del(`/api/v1/teams/${id}`)
      .expect(200)

    expect(get).toEqual({
      payload: {
        data: {
          affected: 1,
          raw: [],
        },
      },
    })
  })

  it('DELETE teams/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .del(`/api/v1/teams/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('DELETE teams/:id error handler', async () => {
    sandbox
      .stub(teamOperation.prototype, 'remove')
      .rejects(new Error('Test error'))

    const team = makeFakeNewTeam()
    const { body: post } = await request(app)
      .post('/api/v1/teams/')
      .send(team)
      .expect(200)

    const { body: del } = await request(app)
      .del(`/api/v1/teams/${post.payload.data.id}`)
      .expect(500)

    const { name, status, message } = del

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })
})
