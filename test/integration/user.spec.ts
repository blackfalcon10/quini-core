import DatabaseBootstrap from '../../src/bootstrap/database.bootstrap'
import request from 'supertest'
import app from '../../src/app'
import { makeFakeNewUser } from '../mocks/user.mock'
import sinon from 'sinon'
import userOperation from '../../src/modules/user/infraestructure/user.operation'

const databaseBootstrap = new DatabaseBootstrap()

describe('users', () => {
  beforeAll(async () => {
    await databaseBootstrap.initialize()
  })

  beforeEach(async () => {
    await databaseBootstrap.cleanDb()
  })

  afterAll(async () => {
    await databaseBootstrap.getConnection().close()
  })

  it('POST users', async () => {
    const user = makeFakeNewUser()
    const { body } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(201)

    const { name, email, photo, id } = body.payload.data

    expect(name).toBe(user.name)
    expect(email).toBe(user.email)
    expect(photo).toBe(user.photo)
    expect(id).toBeTruthy()
  })

  it('GET users empty values', async () => {
    const response = await request(app).get('/api/v1/users').expect(200)

    expect(response.body.payload).toEqual({ data: [] })
  })

  it('GET users with values', async () => {
    const user = makeFakeNewUser()
    const { body: post } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(201)

    const { body: get } = await request(app).get('/api/v1/users').expect(200)

    expect(get.payload).toEqual({ data: [post.payload.data] })
  })

  it('GET users/:id with empty values', async () => {
    const userId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/users/${userId}`)
      .expect(200)

    expect(get).toEqual({
      payload: {},
    })
  })

  it('GET users/:id with with values', async () => {
    const user = makeFakeNewUser()
    const { body: post } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(201)

    const { data } = post.payload

    const { body: get } = await request(app)
      .get(`/api/v1/users/${data.id}`)
      .expect(200)

    expect(get.payload).toEqual({
      data,
    })
  })

  it('POST users validations', async () => {
    const user = makeFakeNewUser()
    delete user.name
    const { body } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(body).toEqual(["'name' is required"])
  })

  it('POST users validate if email parse lowercase', async () => {
    const user = makeFakeNewUser({ email: 'TesT@gmail.com' })
    const { body } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(201)

    const { name, email, photo, id } = body.payload.data

    expect(name).toBe(user.name)
    expect(email).toBe(user.email.toLocaleLowerCase())
    expect(photo).toBe(user.photo)
    expect(id).toBeTruthy()
  })

  it('POST users validate if email already exists', async () => {
    const user = makeFakeNewUser()
    await request(app).post('/api/v1/users/').send(user).expect(201)

    const { body } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(400)

    expect(body).toEqual({ message: 'user already exists' })
  })

  it('GET users/:id validation of the id', async () => {
    const invalidId = 'abc'
    const { body: get } = await request(app)
      .get(`/api/v1/users/${invalidId}`)
      .expect(400)

    // eslint-disable-next-line quotes
    expect(get).toEqual(["'id' must be a number"])
  })

  it('GET users error handler', async () => {
    sinon.stub(userOperation.prototype, 'list').rejects(new Error('Test error'))

    const { body: get } = await request(app).get('/api/v1/users').expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('GET users/:id error handler', async () => {
    sinon
      .stub(userOperation.prototype, 'listOne')
      .rejects(new Error('Test error'))

    const userId = 1
    const { body: get } = await request(app)
      .get(`/api/v1/users/${userId}`)
      .expect(500)

    const { name, status, message } = get

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })

  it('POST users error handler', async () => {
    const user = makeFakeNewUser()
    sinon
      .stub(userOperation.prototype, 'insert')
      .rejects(new Error('Test error'))

    const { body: post } = await request(app)
      .post('/api/v1/users/')
      .send(user)
      .expect(500)

    const { name, status, message } = post

    expect(name).toBe('Error')
    expect(status).toBe(500)
    expect(message).toBe('Test error')
  })
})
