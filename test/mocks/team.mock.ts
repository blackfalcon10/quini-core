import Result from '../../src/modules/shared/application/result.interface'
import TeamModel from '../../src/modules/team/domain/team.model'

const makeFakeTeam = ({
  id = 1,
  name = 'Toluca',
  shield = 'profile.jpg',
} = {}): TeamModel => ({
  id,
  name,
  shield,
})
const makeFakeNewTeam = ({ name = 'Chivas', shield = 'chivas.jpg' } = {}) => ({
  name,
  shield,
})

const makeFakeTeams = (teams: Array<TeamModel> = []): Result<TeamModel> => ({
  payload: {
    data: teams,
  },
})

export { makeFakeTeam, makeFakeTeams, makeFakeNewTeam }
