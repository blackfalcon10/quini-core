import Result from '../../src/modules/shared/application/result.interface'
import LeagueModel from '../../src/modules/league/domain/league.model'

const makeFakeLeague = ({ id = 1, name = 'Liga MX' } = {}): LeagueModel => ({
  id,
  name,
})
const makeFakeNewLeague = ({ name = 'Liga MX' } = {}) => ({
  name,
  status: true,
  isFree: false,
})

const makeFakeLeagues = (
  leagues: Array<LeagueModel> = []
): Result<LeagueModel> => ({
  payload: {
    data: leagues,
  },
})

export { makeFakeLeague, makeFakeLeagues, makeFakeNewLeague }
