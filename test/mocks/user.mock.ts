import Result from '../../src/modules/shared/application/result.interface'
import UserModel from '../../src/modules/user/domain/user.model'

const makeFakeUser = ({
  id = 1,
  name = 'Test',
  email = 'test@gmail.com',
  password = '$2abcde.',
  photo = 'profile.jpg',
  refreshToken = '123',
  active = true,
} = {}): UserModel => ({
  id,
  name,
  email,
  password,
  photo,
  refreshToken,
  active,
})

const makeFakeNewUser = ({
  name = 'Test',
  email = 'test@gmail.com',
  password = 'Black105#',
  photo = 'profile.jpg',
} = {}) => ({
  name,
  email,
  password,
  photo,
})

const makeFakeUsers = (users: Array<UserModel> = []): Result<UserModel> => ({
  payload: {
    data: users,
  },
})

export { makeFakeUser, makeFakeUsers, makeFakeNewUser }
