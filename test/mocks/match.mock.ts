import Result from '../../src/modules/shared/application/result.interface'
import MatchModel from '../../src/modules/match/domain/match.model'
import { makeFakeNewMatchday } from './matchday.mock'
import moment from 'moment'
import { makeFakeTeam } from './team.mock'

const makeFakeMatch = ({
  localId = 1,
  localResult = 3,
  visitorId = 2,
  visitorResult = 4,
} = {}): MatchModel => ({
  local: {
    teamId: makeFakeTeam({ id: localId }),
    result: localResult,
  },
  visitor: {
    teamId: makeFakeTeam({ id: visitorId }),
    result: visitorResult,
  },
  endDate: moment('2022-08-30').toDate(),
  matchday: makeFakeNewMatchday(),
  status: false,
})
const makeFakeNewMatch = ({
  localId = 1,
  localResult = 3,
  visitorId = 2,
  visitorResult = 4,
} = {}) => ({
  local: {
    teamId: localId,
    result: localResult,
  },
  visitor: {
    teamId: visitorId,
    result: visitorResult,
  },
  endDate: moment('2022-08-30').toDate(),
  matchday: makeFakeNewMatchday(),
  status: false,
})

const makeFakeMatchs = (
  matchs: Array<MatchModel> = []
): Result<MatchModel> => ({
  payload: {
    data: matchs,
  },
})

export { makeFakeMatch, makeFakeMatchs, makeFakeNewMatch }
