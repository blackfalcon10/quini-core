import Result from '../../src/modules/shared/application/result.interface'
import MatchdayModel from '../../src/modules/matchday/domain/matchday.model'

const makeFakeMatchday = ({ id = 1, num = 1 } = {}): MatchdayModel => ({
  id,
  num,
  status: false,
  league: {
    id: 1,
    name: 'LIGA MX APERTURA 2027',
    status: false,
    isFree: true,
  },
})
const makeFakeNewMatchday = ({ num = 1 } = {}) => ({
  num,
  status: false,
  league: {
    id: 1,
    name: 'LIGA MX APERTURA 2027',
    status: false,
    isFree: true,
  },
})

const makeFakeMatchdays = (
  matchdays: Array<MatchdayModel> = []
): Result<MatchdayModel> => ({
  payload: {
    data: matchdays,
  },
})

export { makeFakeMatchday, makeFakeMatchdays, makeFakeNewMatchday }
