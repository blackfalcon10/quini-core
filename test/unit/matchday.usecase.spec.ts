import MatchdayOperation from '../../src/modules/matchday/infraestructure/matchday.operation'
import MatchdayUseCase from '../../src/modules/matchday/application/matchday.usecase'
import {
  makeFakeMatchday,
  makeFakeMatchdays,
  makeFakeNewMatchday,
} from '../mocks/matchday.mock'

describe('matchday usecase', () => {
  it('insert: should save a new matchday', async () => {
    const matchday = makeFakeNewMatchday()
    const newMatchday = {
      payload: {
        data: {
          ...matchday,
          id: 1,
        },
      },
    }
    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      insert: jest.fn().mockResolvedValue(newMatchday),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)
    const response = await matchdayUseCase.insert(matchday)

    expect(response).toBe(newMatchday)
    expect(operationMatchday.insert).toHaveBeenLastCalledWith(matchday)
  })

  it('Update: matchday', async () => {
    const matchday = makeFakeMatchday()
    const matchdayMock = {
      payload: {
        data: matchday,
      },
    }
    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      update: jest.fn().mockResolvedValue(matchdayMock),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)

    const response = await matchdayUseCase.update(matchday, { id: matchday.id })

    expect(response).toBe(matchdayMock)
    expect(operationMatchday.update).toHaveBeenLastCalledWith(
      matchday,
      { id: matchday.id },
      []
    )
  })

  it('list: return all matchdays empty', async () => {
    const matchdays = makeFakeMatchdays()
    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(matchdays),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)
    const response = await matchdayUseCase.list()

    expect(response.payload.data).toEqual(matchdays.payload.data)
    expect(operationMatchday.list).toHaveBeenCalledTimes(1)
  })

  it('list: return all matchdays with data', async () => {
    const matchday1 = makeFakeMatchday()
    const matchday2 = makeFakeMatchday({
      num: 2,
    })
    const matchday3 = makeFakeMatchday({ num: 3 })
    const matchdays = makeFakeMatchdays([matchday1, matchday2, matchday3])

    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(matchdays),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)
    const response = await matchdayUseCase.list()

    expect(response.payload.data).toEqual(matchdays.payload.data)
    expect(operationMatchday.list).toHaveBeenCalledTimes(1)
  })

  it('listOne: return matchday by id', async () => {
    const matchday = makeFakeMatchday()

    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      listOne: jest.fn().mockResolvedValue({
        payload: {
          data: matchday,
        },
      }),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)
    const response = await matchdayUseCase.listOne({ id: matchday.id })

    expect(response.payload.data).toEqual(matchday)
    expect(operationMatchday.listOne).toHaveBeenCalledTimes(1)
  })

  it('Delete: matchday by id', async () => {
    const matchday = makeFakeMatchday()

    ;(MatchdayOperation as jest.Mock) = jest.fn().mockReturnValue({
      remove: jest.fn().mockResolvedValue({
        payload: {
          data: matchday,
        },
      }),
    })

    const operationMatchday = new MatchdayOperation()

    const matchdayUseCase = new MatchdayUseCase(operationMatchday)
    const response = await matchdayUseCase.remove({ id: matchday.id })

    expect(response.payload.data).toEqual(matchday)
    expect(operationMatchday.remove).toHaveBeenCalledTimes(1)
  })
})
