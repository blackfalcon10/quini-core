import LeagueOperation from '../../src/modules/league/infraestructure/league.operation'
import LeagueUseCase from '../../src/modules/league/application/league.usecase'
import {
  makeFakeLeague,
  makeFakeLeagues,
  makeFakeNewLeague,
} from '../mocks/league.mock'

describe('league usecase', () => {
  it('insert: should save a new league', async () => {
    const league = makeFakeNewLeague()
    const newLeague = {
      payload: {
        data: {
          ...league,
          id: 1,
        },
      },
    }
    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      insert: jest.fn().mockResolvedValue(newLeague),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)
    const response = await leagueUseCase.insert(league)

    expect(response).toBe(newLeague)
    expect(operationLeague.insert).toHaveBeenLastCalledWith(league)
  })

  it('Update: league', async () => {
    const league = makeFakeLeague()
    const leagueMock = {
      payload: {
        data: league,
      },
    }
    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      update: jest.fn().mockResolvedValue(leagueMock),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)

    const response = await leagueUseCase.update(league, { id: league.id })

    expect(response).toBe(leagueMock)
    expect(operationLeague.update).toHaveBeenLastCalledWith(
      league,
      { id: league.id },
      []
    )
  })

  it('list: return all leagues empty', async () => {
    const leagues = makeFakeLeagues()
    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(leagues),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)
    const response = await leagueUseCase.list()

    expect(response.payload.data).toEqual(leagues.payload.data)
    expect(operationLeague.list).toHaveBeenCalledTimes(1)
  })

  it('list: return all leagues with data', async () => {
    const league1 = makeFakeLeague()
    const league2 = makeFakeLeague({
      name: 'Liga Argentina',
    })
    const league3 = makeFakeLeague({ name: 'Premier League' })
    const leagues = makeFakeLeagues([league1, league2, league3])

    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(leagues),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)
    const response = await leagueUseCase.list()

    expect(response.payload.data).toEqual(leagues.payload.data)
    expect(operationLeague.list).toHaveBeenCalledTimes(1)
  })

  it('listOne: return league by id', async () => {
    const league = makeFakeLeague()

    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      listOne: jest.fn().mockResolvedValue({
        payload: {
          data: league,
        },
      }),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)
    const response = await leagueUseCase.listOne({ id: league.id })

    expect(response.payload.data).toEqual(league)
    expect(operationLeague.listOne).toHaveBeenCalledTimes(1)
  })

  it('Delete: league by id', async () => {
    const league = makeFakeLeague()

    ;(LeagueOperation as jest.Mock) = jest.fn().mockReturnValue({
      remove: jest.fn().mockResolvedValue({
        payload: {
          data: league,
        },
      }),
    })

    const operationLeague = new LeagueOperation()

    const leagueUseCase = new LeagueUseCase(operationLeague)
    const response = await leagueUseCase.remove({ id: league.id })

    expect(response.payload.data).toEqual(league)
    expect(operationLeague.remove).toHaveBeenCalledTimes(1)
  })
})
