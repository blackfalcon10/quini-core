import MatchOperation from '../../src/modules/match/infraestructure/match.operation'
import MatchUseCase from '../../src/modules/match/application/match.usecase'
import { makeFakeMatch, makeFakeMatchs } from '../mocks/match.mock'

describe('match usecase', () => {
  it('insert: should save a new match', async () => {
    const match = makeFakeMatch()
    const newMatch = {
      payload: {
        data: {
          ...match,
          id: 1,
        },
      },
    }
    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      insert: jest.fn().mockResolvedValue(newMatch),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)
    const response = await matchUseCase.insert(match)

    expect(response).toBe(newMatch)
    expect(operationMatch.insert).toHaveBeenLastCalledWith(match)
  })

  it('Update: match', async () => {
    const match = makeFakeMatch()
    const matchMock = {
      payload: {
        data: match,
      },
    }
    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      update: jest.fn().mockResolvedValue(matchMock),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)

    const response = await matchUseCase.update(match, { id: match.id })

    expect(response).toBe(matchMock)
    expect(operationMatch.update).toHaveBeenLastCalledWith(
      match,
      { id: match.id },
      []
    )
  })

  it('list: return all matchs empty', async () => {
    const matchs = makeFakeMatchs()
    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(matchs),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)
    const response = await matchUseCase.list()

    expect(response.payload.data).toEqual(matchs.payload.data)
    expect(operationMatch.list).toHaveBeenCalledTimes(1)
  })

  it('list: return all matchs with data', async () => {
    const match1 = makeFakeMatch()
    const match2 = makeFakeMatch({
      localId: 3,
      visitorId: 4,
    })
    const match3 = makeFakeMatch({
      localId: 5,
      visitorId: 6,
    })
    const matchs = makeFakeMatchs([match1, match2, match3])

    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(matchs),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)
    const response = await matchUseCase.list()

    expect(response.payload.data).toEqual(matchs.payload.data)
    expect(operationMatch.list).toHaveBeenCalledTimes(1)
  })

  it('listOne: return match by id', async () => {
    const match = makeFakeMatch()

    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      listOne: jest.fn().mockResolvedValue({
        payload: {
          data: match,
        },
      }),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)
    const response = await matchUseCase.listOne({ id: match.id })

    expect(response.payload.data).toEqual(match)
    expect(operationMatch.listOne).toHaveBeenCalledTimes(1)
  })

  it('Delete: match by id', async () => {
    const match = makeFakeMatch()

    ;(MatchOperation as jest.Mock) = jest.fn().mockReturnValue({
      remove: jest.fn().mockResolvedValue({
        payload: {
          data: match,
        },
      }),
    })

    const operationMatch = new MatchOperation()

    const matchUseCase = new MatchUseCase(operationMatch)
    const response = await matchUseCase.remove({ id: match.id })

    expect(response.payload.data).toEqual(match)
    expect(operationMatch.remove).toHaveBeenCalledTimes(1)
  })
})
