import UserOperation from '../../src/modules/user/infraestructure/user.operation'
import UserUseCase from '../../src/modules/user/application/user.usecase'
import UserService from '../../src/modules/user/application/user.service'
import {
  makeFakeUser,
  makeFakeUsers,
  makeFakeNewUser,
} from '../mocks/user.mock'

jest.mock('../../src/modules/user/application/user.service')

describe('user usecase', () => {
  it('insert: should save a new user', async () => {
    const user = makeFakeNewUser()

    const pass = '$2abcde.1'
    const refresh = '123'
    const newUser = {
      payload: {
        data: {
          ...user,
          id: 1,
          password: pass,
          refreshToken: refresh,
          active: true,
        },
      },
    }
    const mockedCryptPassword = UserService.cryptPassword as jest.Mock
    const refreshToken = UserService.generateRefreshToken as jest.Mock
    mockedCryptPassword.mockReturnValueOnce(pass)
    refreshToken.mockReturnValueOnce(refresh)
    ;(UserOperation as jest.Mock) = jest.fn().mockReturnValue({
      insert: jest.fn().mockResolvedValue(newUser),
    })

    const operationUser = new UserOperation()

    const userUseCase = new UserUseCase(operationUser)
    const response = await userUseCase.insert(user)

    expect(response).toBe(newUser)
    expect(operationUser.insert).toHaveBeenLastCalledWith(user)
  })

  it('list: return all users empty', async () => {
    const users = makeFakeUsers()
    ;(UserOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(users),
    })

    const operationUser = new UserOperation()

    const userUseCase = new UserUseCase(operationUser)
    const response = await userUseCase.list()

    expect(response.payload.data).toEqual(users.payload.data)
    expect(operationUser.list).toHaveBeenCalledTimes(1)
  })

  it('list: return all users with data', async () => {
    const user1 = makeFakeUser()
    const user2 = makeFakeUser({ name: 'Gera', email: 'gera@test.com' })
    const user3 = makeFakeUser({ name: 'Edgar', email: 'edgar@test.com' })
    const users = makeFakeUsers([user1, user2, user3])

    ;(UserOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(users),
    })

    const operationUser = new UserOperation()

    const userUseCase = new UserUseCase(operationUser)
    const response = await userUseCase.list()

    expect(response.payload.data).toEqual(users.payload.data)
    expect(operationUser.list).toHaveBeenCalledTimes(1)
  })

  it('listOne: return user by id', async () => {
    const user = makeFakeUser()

    ;(UserOperation as jest.Mock) = jest.fn().mockReturnValue({
      listOne: jest.fn().mockResolvedValue({
        payload: {
          data: user,
        },
      }),
    })

    const operationUser = new UserOperation()

    const userUseCase = new UserUseCase(operationUser)
    const response = await userUseCase.listOne({ id: user.id })

    expect(response.payload.data).toEqual(user)
    expect(operationUser.listOne).toHaveBeenCalledTimes(1)
  })
})
