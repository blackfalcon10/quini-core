import TeamOperation from '../../src/modules/team/infraestructure/team.operation'
import TeamUseCase from '../../src/modules/team/application/team.usecase'
import {
  makeFakeTeam,
  makeFakeTeams,
  makeFakeNewTeam,
} from '../mocks/team.mock'

describe('team usecase', () => {
  it('insert: should save a new team', async () => {
    const team = makeFakeNewTeam()
    const newTeam = {
      payload: {
        data: {
          ...team,
          id: 1,
        },
      },
    }
    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      insert: jest.fn().mockResolvedValue(newTeam),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)
    const response = await teamUseCase.insert(team)

    expect(response).toBe(newTeam)
    expect(operationTeam.insert).toHaveBeenLastCalledWith(team)
  })

  it('Update: team', async () => {
    const team = makeFakeTeam()
    const teamMock = {
      payload: {
        data: team,
      },
    }
    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      update: jest.fn().mockResolvedValue(teamMock),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)

    const response = await teamUseCase.update(team, { id: team.id })

    expect(response).toBe(teamMock)
    expect(operationTeam.update).toHaveBeenLastCalledWith(
      team,
      { id: team.id },
      []
    )
  })

  it('list: return all teams empty', async () => {
    const teams = makeFakeTeams()
    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(teams),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)
    const response = await teamUseCase.list()

    expect(response.payload.data).toEqual(teams.payload.data)
    expect(operationTeam.list).toHaveBeenCalledTimes(1)
  })

  it('list: return all teams with data', async () => {
    const team1 = makeFakeTeam()
    const team2 = makeFakeTeam({ name: 'Monterrey', shield: 'monterrey.png' })
    const team3 = makeFakeTeam({ name: 'Chema', shield: 'chema.png' })
    const teams = makeFakeTeams([team1, team2, team3])

    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      list: jest.fn().mockResolvedValue(teams),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)
    const response = await teamUseCase.list()

    expect(response.payload.data).toEqual(teams.payload.data)
    expect(operationTeam.list).toHaveBeenCalledTimes(1)
  })

  it('listOne: return team by id', async () => {
    const team = makeFakeTeam()

    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      listOne: jest.fn().mockResolvedValue({
        payload: {
          data: team,
        },
      }),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)
    const response = await teamUseCase.listOne({ id: team.id })

    expect(response.payload.data).toEqual(team)
    expect(operationTeam.listOne).toHaveBeenCalledTimes(1)
  })

  it('Delete: team by id', async () => {
    const team = makeFakeTeam()

    ;(TeamOperation as jest.Mock) = jest.fn().mockReturnValue({
      remove: jest.fn().mockResolvedValue({
        payload: {
          data: team,
        },
      }),
    })

    const operationTeam = new TeamOperation()

    const teamUseCase = new TeamUseCase(operationTeam)
    const response = await teamUseCase.remove({ id: team.id })

    expect(response.payload.data).toEqual(team)
    expect(operationTeam.remove).toHaveBeenCalledTimes(1)
  })
})
