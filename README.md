# Core QuiniMx

Core to quinimx.com

## Getting Started

### Prerequisites

What things you need to install the software and how to install them

- NodeJS v16
- npm v8
- Git

### Dockers

#### Mysql
```
    $ docker run -d --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=test -e MYSQL_DATABASE=test -e MYSQL_ROOT_HOST=%  mysql/mysql-server:8.0
```

### Installing

1. Install the node dependencies
    ```
        $ npm install
    ```
2. Copy env.example.yaml file to env.yaml
    ```
        $ cp env.example.yaml env.yaml
    ```
3. Start the project
    ```
        $ npm run develop
    ```

- ### Sonarqube
		````
		sonar-scanner \
			-Dsonar.projectKey=quini-core \
			-Dsonar.sources=. \
			-Dsonar.host.url=http://localhost:8084 \
			-Dsonar.login=9f58b810f73f5d55f9f19b3aaf30757cb7b47a09
		````

- ### View to Mysql

Query:
```
SELECT
	`u`.`id` AS `id`,
	`u`.`name` AS `name`,
	`u`.`avatar` AS `avatar`,
	sum( `s`.`win` ) AS `wins`,
	sum( `s`.`lost` ) AS `lost`,
	sum( `s`.`score` ) AS `score`,
	count( `u`.`id` ) AS `jj`
FROM
	`score` `s`
	LEFT JOIN `user` `u` ON  `u`.`id` = `s`.`userId`
	LEFT JOIN `matchday` `m` ON `m`.`id` = `s`.`matchdayId`
WHERE
	`m`.`leagueId` = 1
GROUP BY
	`u`.`id`
ORDER BY
	sum( `s`.`win` ) DESC,
	sum( `s`.`score` ) DESC,
	`u`.`name`
```
