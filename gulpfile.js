const uglify = require('gulp-uglify')
const { src, dest } = require('gulp')

const minify = () => {
  return src('./cache/**/*.js').pipe(uglify()).pipe(dest('dist'))
}

exports.minify = minify
